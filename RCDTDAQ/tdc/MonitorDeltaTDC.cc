#include <TH1.h>
#include <TFile.h>

#include <oh/OHRootProvider.h>

#include "MonitorDeltaTDC.h"
#include "BL4SConfig.h"
#include "CommonLibrary/RCDRawEvent.h"

using namespace std;

//************************** TDC Module number hardwired   *********
  const int TDCNo = 0;
  const int V1290_CHANNELS = 16;
  const double Tbin = 0.025;   // ns
//******************************************************************

MonitorDeltaTdc::MonitorDeltaTdc(std::string name, BL4SConfig * cfg, 
                             std::string runNumberStr, bool publish )
{
  m_MonitorName = name;

  // BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

  m_nScin   = cfg->GetNScintillator();
  std::cout << " MonitorDeltaTdc:: " << " m_nScin = " << m_nScin << std::endl;

/********booking histograms**************/
/* copied from last year ...  */

    for(int i=1; i<m_nScin; i++) {
      char name[100], h_title[50];
      sprintf(name,"scin%dtime-scin0time",i);
      sprintf(h_title,"SC%d - SC0 time difference; time(ns); # of hits",i);
      m_scin_time.push_back(new TH1D(name,h_title ,500,-200,100));
    }

}

MonitorDeltaTdc::~MonitorDeltaTdc()
{
}

bool
MonitorDeltaTdc::Process(RCDRawEvent& ev)
{
  if (ev.v1290_data.size() != 1 || ev.v1290_data[0].Channel.size() < m_nScin){
    return false;
  }
  for(int i=1; i<m_nScin;i++) {
    double sc0 = ev.v1290_data[0].Channel[0].Data * Tbin;
    double diff = ev.v1290_data[0].Channel[i].Data * Tbin - sc0;
    m_scin_time[i-1]->Fill(diff);
  }

 return true;
}

void
MonitorDeltaTdc::Print(RCDRawEvent& ev)
{
  cout << std::dec << "TDC Modules: " << ev.v1290_data.size() << "\n";
  double sc0 = ev.v1290_data[0].Channel[0].Data * Tbin;

  for (int i=1; i<m_nScin;i++) {
    double diff = ev.v1290_data[0].Channel[i].Data * Tbin - sc0;
    std::cout << " Print " << " channel = " << i << " sc - sc0 (ns) = " << diff << std::endl;
  }
}

void
MonitorDeltaTdc::WriteToFile() {

  m_outputFile->Write();
  m_outputFile->Close();
}

void
MonitorDeltaTdc::PublishHists() {
  
  for (int i=1; i<m_nScin;i++) {
    m_histProvider->publish(*m_scin_time[i-1],m_scin_time[i-1]->GetName());
  }

}

void MonitorDeltaTdc::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "DeltaTdc_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorChamber::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();    // current directory

}

