//#include <boost/format.hpp>
#include <vector>

#include <oh/OHRootProvider.h>

#include "EventLooper.h"
#include "MonitorTDC.h"
#include "MonitorDeltaTDC.h"
#include "CommonLibrary/DAQEventReader.h"
#include "CommonLibrary/FileEventReader.h"
#include "CommonLibrary/RCDRawEvent.h"

EventLooper::EventLooper():
  m_histProvider(NULL),
  m_publishHists(false),
  m_publishCycleTime(30),
  m_dataFile("")
{
  m_lastPublicationTime = time(NULL);
}

template <typename EventReader, typename Source>
void 
EventLooper::ProcessEvents(Source src, int nEvents, bool multirun)
{
  EventReader evReader(src,multirun);
  cout << " EventLooper::ProcessEvents " << " run number = " << evReader.GetRunNumber() << endl;
  cout << " EventLooper::ProcessEvents " << " m_publishHists = " << m_publishHists << endl;

  std::vector<std::unique_ptr<MonitorBase>> vMonitors;

  std::unique_ptr<MonitorTdc> mTdc(new MonitorTdc("mTdc", m_cfg, to_string(evReader.GetRunNumber()), m_publishHists));
  // transmit to Monitor 
  mTdc->SetHistProvider(m_histProvider);
 
  vMonitors.push_back(std::move(mTdc));

  std::unique_ptr<MonitorDeltaTdc> mDeltaTdc(new MonitorDeltaTdc("mDeltaTdc", m_cfg, to_string(evReader.GetRunNumber()), m_publishHists));
  // transmit to Monitor 
  mDeltaTdc->SetHistProvider(m_histProvider);

  vMonitors.push_back(std::move(mDeltaTdc));
 
  cout << " # monitors = " << vMonitors.size() << endl;

  cout << "Number of events: " << nEvents << endl;
  int eventCount = 0;
  while (eventCount < nEvents || nEvents == -1) {
//    cout << "\n # Event No.: " << eventCount << endl;
    vector<unsigned int> rawEvent = evReader.GetNextRawEvent();
//    std::cout << "EventLooper::ProcessEvents " << " rawEvent size = " << rawEvent.size() << std::endl;
    int evSize = rawEvent.size();
    if(rawEvent.size() == 0)  // End Of File
      break;
    // skip events with status ne 0
    int status = rawEvent[evSize-4];  // REVIEW with new FileEventReader
    if (status !=0) {
      std::cout << " status = " << status << " event size = " << evSize << " eventCount = " << eventCount << std::endl;
      continue;
    }

    RCDRawEvent ev = RCDRawEvent(rawEvent);
    if ( ! ev.isValid() ){
      continue;
    }

//    for(const unsigned int& it : rawEvent) {
//      cout << boost::format("%08x") % it << " ";
//    }

    for(auto & monitor : vMonitors) {
      monitor->Process(ev);
//      monitor->Print(ev);
    }

    if(m_publishHists && IsTimeToPublish())
    {
      for(auto & monitor : vMonitors)
      monitor->PublishHists();
    }
    ++eventCount;
  }

  if (!m_publishHists) {
    for(auto & monitor : vMonitors) {
      monitor->WriteToFile();
      std::cout << "EventLooper::ProcessEvents " << " ROOT file written " << std::endl;
    }
  }
}

template void EventLooper::ProcessEvents<DAQEventReader, IPCPartition>(IPCPartition, int, bool);
template void EventLooper::ProcessEvents<FileEventReader, std::string>(std::string, int, bool);

bool EventLooper::IsTimeToPublish()
{
  time_t currentTime = time(NULL);
  if( static_cast<long int>(currentTime-m_lastPublicationTime) < m_publishCycleTime)
    return false;
  m_lastPublicationTime = time(NULL);
  return true;
}
