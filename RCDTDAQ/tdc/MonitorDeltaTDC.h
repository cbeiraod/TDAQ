#ifndef __MONITOR_DELTA_TDC__ 
#define __MONITOR_DELTA_TDC__ 

#include <vector>

#include "CommonLibrary/MonitorBase.h"

class BL4SConfig;
class OHRootProvider;
class TH1D;
class TFile;

class MonitorDeltaTdc: public MonitorBase {
  public:
    MonitorDeltaTdc();
    MonitorDeltaTdc(std::string name, BL4SConfig* cfg, std::string runNumber, bool publish);
    ~MonitorDeltaTdc();

    virtual bool Process(RCDRawEvent& ev);
    virtual void Print(RCDRawEvent& ev);
    virtual void WriteToFile();
    virtual void PublishHists();
    virtual void PrepareRootFile(std::string runNumberStr);

  private:
    TFile * m_outputFile;
    std::string m_MonitorName;

    int m_nScin;
    std::vector<TH1D*> m_scin_time;

};

  
#endif
