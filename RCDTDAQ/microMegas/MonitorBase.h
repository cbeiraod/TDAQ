#ifndef __MONITOR_BASE_H__
#define __MONITOR_BASE_H__

#include "RCDRawEvent.h"

#include <vector>

class MonitorBase {
  public:

//    virtual bool Process(RCDRawEvent& ev) = 0;
//    virtual void Print(RCDRawEvent& ev) = 0;
    virtual void WriteToFile() = 0;
    virtual void PrepareRootFile(std::string runNumberStr) = 0;

    virtual bool Process(std::vector<unsigned int>& ev) = 0;
    virtual void PublishHists() = 0;

    std::string GetMonitorName() { return fMonitorName; }

  private:
    // virtual void ResetVars() {};

  protected:
    std::string fMonitorName;

};

#endif
