#include <utility>
#include <string>
#include <vector>
#include "CDetChamber.h"
#include "CConfiguration.h"
#include "CEventDecoder.h"

std::pair<unsigned,unsigned> stripRange(CConfiguration* config, std::string chamberName) {   // of THE chamber 

  std::vector<std::string> definedChambers = config->defined_chamber_names();
  unsigned highStrip, lowStrip; 
  for (std::vector<std::string> :: iterator is = definedChambers.begin();
                                            is != definedChambers.end(); ++is) {
    std::cout << " MonitorChamber::stripRange " << " chamber name = " << *is << std::endl;
    if (*is == chamberName) {
      const CDetChamber* chamber = config->chamber_by_name(*is);
      int chamberId = chamber->get_id();
      std::cout << " MonitorChamber::stripRange " << " chamber Id = " << chamberId << std::endl;
      std::pair<unsigned,unsigned> lowHigh = config->defined_chamber_strip_ranges(chamberId);
      std::cout << " MonitorChamber::stripRange " << " lowHigh.first = " << lowHigh.first <<
                " lowHigh.second = " << lowHigh.second << std::endl;
      lowStrip = lowHigh.first;
      highStrip = lowHigh.second;
    }
  }
  return std::pair<unsigned,unsigned>(lowStrip, highStrip);
}
