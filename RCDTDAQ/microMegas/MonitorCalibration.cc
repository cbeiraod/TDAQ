#include "CUDPData.h"
#include "CConfiguration.h"
#include "CDetChamber.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"

#include <oh/OHRootProvider.h>

#include "MonitorCalibration.h"

using namespace std;

int  buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);
std::pair<unsigned,unsigned> stripRange(CConfiguration* config, std::string chamberName);

MonitorCalibration::MonitorCalibration(std::string name, CConfiguration* config, std::string chamberName, int stripNumber,
                               std::string runNumberStr, bool publish)
{
  m_MonitorName = name;
  m_chamberName = chamberName;
  m_stripNumber = stripNumber;
  m_config = config;

  m_firstEvent = true;

  stripRange(m_config, m_chamberName);

  std::cout << " MonitorCalibration::MonitorCalibration " <<  " name = " << (m_chamberName + "_qmax") << std::endl;

  pedestalVectors();

// BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

// defining histograms
// this could also be done in the 'firstEvent' part of Process IF event parameters are needed to define the histos

// assuming no holes ..
  std::cout << " MonitorCalibration::MonitorCalibration " << " # strips = " << 
            (m_highStrip - m_lowStrip + 1) << std::endl;

  std::vector <TH1F*> hStripQmax;
  int ii=0;
  for(int ii=0; ii<(m_highStrip - m_lowStrip + 1); ii++) {
    std::string stripString = std::to_string(ii);
    TH1F* hh = new TH1F((m_chamberName + "_qmaxEvent" + stripString).c_str(),
                      (m_chamberName + " Event Qmax" + stripString).c_str(),
                      400, 0, 4000.0); 
    m_hStripQmax.push_back(hh);
    
  }
  std::cout << " MonitorCalibration::MonitorCalibration " <<  " size of hStripQmax = " << m_hStripQmax.size() << std::endl;

}

MonitorCalibration::~MonitorCalibration()
{
}

bool
MonitorCalibration::Process(vector<unsigned int>& ev)
{
 // cout<<" MonitorCalibration::Process "<<endl;
  int retBuild =buildEventVectors(ev);

  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =          m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =          m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =          m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id =            m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =       m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip    =      m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =    m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();

  if (m_firstEvent) {
    std::cout << " apv_evt = " << apv_evt << std::endl;

    std::cout << " size of apv_fec = " << apv_fec.size()
              << " fecNo[0] = " << apv_fec[0]  << std::endl;

    std::cout << " size of apv_id = " << apv_id.size()
              << " id[0] = " << apv_id[0]
              << " id[1] = " << apv_id[1]
              << " id[127] = " << apv_id[127]
              << " id[129] = " << apv_id[129]  << std::endl;

    std::cout << " size of apv_ch = " << apv_ch.size()
              << " ch[0] = " << apv_ch[0]  << std::endl;

    std::cout << " size of mm_id = " << mm_id.size()
              << " mm_id[0] = " << mm_id[0]
              << " mm_id[257] = " << mm_id[257]  << std::endl;

    std::cout << " size of mm_readout = " << mm_readout.size()
              << " mm_readout[0] = " << mm_readout[0]
              << " mm_readout[257] = " << mm_readout[257]  << std::endl;

    std::cout << " size of mm_strip = " << mm_strip.size()
              << " mm_strip[0] = " << mm_strip[0]
              << " mm_strip[257] = " << mm_strip[257]  << std::endl;

    std::cout << " size of apv_qmax = " << apv_qmax.size()
              << " apv_qmax[0] = " << apv_qmax[0] << " apv_qmax[7] = " << apv_qmax[7]
              << std::endl;

      std::cout << " m_chamberName = " << m_chamberName << std::endl;

    m_firstEvent = false;
  }
 
  int maxQ_event = 0;   	// max Q of event
  int maxStrip_event = 0;	// strip of maxQ_event
  int maxApvCh_event = 0;	// APV channel of maxQ_event
 
// of THE other chamber
  int maxQ_event_second = 0;   	// max Q of event
  int maxStrip_event_second = 0;	// strip of maxQ_event
  int maxApvCh_event_second = 0;	// APV channel of maxQ_event

  int apvChannel = 0;

// the max calculation assumes pedestal corrected data !!
  for (auto row = apv_q.begin(); row != apv_q.end(); ++row, ++apvChannel)
  {
    if (mm_id[apvChannel] == m_chamberName) {
      std::vector<int16_t>::iterator imax = max_element(row->begin(), row->end());	// Q max over timebins in one channel
//      std::cout << " apvChannel = " << apvChannel << " strip = " << mm_strip[apvChannel] << " chamber = " << mm_id[apvChannel]
//                << " Q max = " << *imax << std::endl;
      int tbQmax = std::distance(row->begin(),  imax);
      //std::cout << " tbQmax = " << tbQmax << std::endl;

      int maxQ = *imax;

      if(maxQ > maxQ_event) {
        maxQ_event = maxQ;
        maxStrip_event = mm_strip[apvChannel];
        maxApvCh_event = apvChannel;
      }
    }
    else  {   // the other chamber
      std::vector<int16_t>::iterator imax_second = max_element(row->begin(), row->end());	// Q max over timebins in one channel
//      std::cout << " apvChannel = " << apvChannel << " strip = " << mm_strip[apvChannel] << " chamber = " << mm_id[apvChannel]
//                << " Q max = " << *imax_second << std::endl;

      int maxQ_second = *imax_second;

      if(maxQ_second > maxQ_event_second) {
        maxQ_event_second = maxQ_second;
        maxStrip_event_second = mm_strip[apvChannel];
        maxApvCh_event_second = apvChannel;
      }
    }
  }

// define sigma_cut_factor in the .h file 
  double  pedstd = m_pedestal_stdev.at(maxApvCh_event);
//  std::cout << " Process " << " maxApvCh_event = " << maxApvCh_event
//            << " pedStd = " <<  pedstd << std::endl;
  double  pedstd_second = m_pedestal_stdev.at(maxApvCh_event_second);
//  std::cout << " Process " << " maxApvCh_event_second = " << maxApvCh_event_second
//            << " pedStd_second = " <<  pedstd_second << std::endl;

// hits in both chambers
  if ((maxQ_event > (pedstd*m_sigma_cut_factor)) &&
      (maxQ_event_second > (pedstd_second*m_sigma_cut_factor))) {   // mean pedestal already subtracted
    m_hStripQmax.at(maxStrip_event-1)->Fill(maxQ_event);
    std::cout << " strip number = " << maxStrip_event << " maxQ event = " << maxQ_event << std::endl;
  }

 return true;
}

void
MonitorCalibration::Print(std::vector<unsigned int>& ev)
{
}

void
MonitorCalibration::WriteToFile() {

  std::cout << " MonitorCalibration::WriteToFile " << std::endl;

  TGraph* qMaxMean = new TGraph();
  qMaxMean->SetTitle((m_chamberName + " Mean of event Qmax vs. strips;strip number; mean of event QMax").c_str());

  for (int ii=0; ii<m_hStripQmax.size(); ii++) {
    double mean = m_hStripQmax[ii]->GetMean();
//    std::cout << " WriteToFile " << " index = " << ii
//              << " mean of QmaxEvent = " << mean << std::endl;
    qMaxMean->SetPoint(ii,ii,mean);
  }
  qMaxMean->Write("meanQmaxEvent");

  TGraph* qMaxEntries = new TGraph();
  qMaxEntries->SetTitle((m_chamberName + " # entries of event Qmax vs. strips;strip number; # entries of event QMax").c_str());

  for (int ii=0; ii<m_hStripQmax.size(); ii++) {
    int nEntries = m_hStripQmax[ii]->GetEntries();
    std::cout << " WriteToFile " << " index = " << ii
              << "  # entries of QmaxEvent = " << nEntries << std::endl;
    qMaxEntries->SetPoint(ii,ii,nEntries);
  }
  qMaxEntries->Write("EntriesQmaxEvent");

//  m_outputFile->Write();
  m_outputFile->Close();

  delete qMaxMean;
  delete qMaxEntries;
  std::cout << " MonitorCalibration::WriteToFile " << " Closed file " << std::endl;
}

void
MonitorCalibration::PublishHists() {

//m_histProvider->publish(*m_maxQ_event,m_maxQ_event->GetName());

}

int MonitorCalibration::buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

  buildFrameContainer(rawEvent,m_datacontainerPtr );
//  std::cout << " buildEventVectors " << " size of data container = " << (*m_datacontainerPtr).size() << std::endl;

   err = m_decoder->buildEventContainer();
//  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

//  std::cout << " ProcessEvents " << " size of event container = " << (*m_eventcontainerPtr).size() << std::endl;

  err = m_eventVectors->buildVectors();

//  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;
}

void MonitorCalibration::pedestalVectors() {

  const std::map<int, double>* pedestals_mean =  m_config->pedestal_mean_map();
  const std::map<int, double>* pedestals_stdev =  m_config->pedestal_stdev_map();
  std::cout << " MonitorCalibration::pedestalVectors " << " size of pedestals_mean = " << pedestals_mean->size() << std::endl;

// vectorize the maps, indexed by 'sequential' channel number as apv vectors
  int ii = 0;
//  std::cout << " Means " << std::endl;
  for(auto it = pedestals_mean->begin(); it != pedestals_mean->end(); ++it, ii++ ) {
//    std::cout << it->first << " " << it->second << "\n";
    m_pedestal_mean.push_back(it->second);
  }

/********************************************************
  for (int jj = 0; jj<m_pedestal_mean.size(); jj++) {
  std::cout << " MonitorCalibration::pedestalVectors " <<  " jj = " << jj
            << " m_pedestal_mean[jj] = "  << m_pedestal_mean[jj] << std::endl;
  }
*********************************************************/

  ii = 0;
//  std::cout << " Stdev s " << std::endl;
  for(auto it = pedestals_stdev->begin(); it != pedestals_stdev->end(); ++it, ii++ ) {
//    std::cout << it->first << " " << it->second << "\n";
    m_pedestal_stdev.push_back(it->second);
  }

/*************************************************
  for (int jj = 0; jj<m_pedestal_stdev.size(); jj++) {
  std::cout << " MonitorCalibration::pedestalVectors " <<  " jj = " << jj
            << " m_pedestal_stdev[jj] = "  << m_pedestal_stdev[jj] << std::endl;
  }
****************************************************/

}


void MonitorCalibration::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Calibration_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorCalibration::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();	// current directory

}
