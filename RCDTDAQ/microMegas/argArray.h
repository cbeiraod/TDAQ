/*************i**************************************/
/*  argArray.h                                      */
/*  common declaration of array dimessions for the  */
/*  argc,argv array passed to the MM configuration  */
/*                                                  */
/*  author:  J.Petersen                             */
/*  2017/03/28                                      */
/***************i************************************/

const int argRows = 10;
const int argColumns = 100;

