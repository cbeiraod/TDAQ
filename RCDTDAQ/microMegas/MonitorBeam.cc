
#include "CUDPData.h"
#include "CConfiguration.h"
#include "CDetChamber.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"

#include <oh/OHRootProvider.h>

#include <TString.h>
#include <TEnv.h>
#include <TAxis.h>

#include "MonitorBeam.h"

using namespace std;

void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);

MonitorBeam::MonitorBeam(std::string name, CConfiguration* config, std::string runNumberStr, bool publish)
{
  m_MonitorName = name;
  m_config = config;

  m_firstEvent = true;

  pedestalVectors();

// BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

  m_chamberNames = m_config->defined_chamber_names(); // rename ..

// get some geometry parameters from BL4SConfig
  getGeometry();
 
// defining histograms
// this could also be done in the 'firstEvent' part of Process IF event parameters are needed to define the histos

  for (auto chamberName: m_config->defined_chamber_names()){
    TH1F* addHist = new TH1F((chamberName + "_xRelative").c_str(),
                             (chamberName + "   x coordinate").c_str(),
                             2000, -250.0, 250.0);    // mm
    addHist->GetXaxis()->SetTitle(" x coordinate relative to middle of chamber (mm)");
    addHist->GetYaxis()->SetTitle(" events");
    m_xRel.push_back(addHist);

  }

  TH2F* corrHist;
// to be checked with more than two chambers ..
  if (m_chamberNames.size() > 1) {
    for (int ii=1; ii<m_chamberNames.size(); ii++) {
      corrHist = new TH2F(("hitmap_"+m_chamberNames[ii-1]+"_"+m_chamberNames[ii]).c_str(),
                          ("hitmap_"+m_chamberNames[ii-1]+"_"+m_chamberNames[ii]).c_str(),
                          1000,-250.0,250.0,1000,-250.0,250.0);
      corrHist->GetXaxis()->SetTitle((m_chamberNames[ii-1] + " coordinate relative to middle of chamber (mm)").c_str());
      corrHist->GetYaxis()->SetTitle((m_chamberNames[ii] + " coordinate relative to middle of chamber (mm)").c_str());
      m_correlation.push_back(corrHist);
    }
  }


}

MonitorBeam::~MonitorBeam()
{
}

bool
MonitorBeam::Process(vector<unsigned int>& ev)
{
 // cout<<" MonitorBeam::Process "<<endl;
  // all APV fragments empty - cannot build vectors
  int retBuild = buildEventVectors(ev);
  if( retBuild != CEventVectors::RETURN_OK) {
    return true;
  }

  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =          m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =          m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =          m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id =            m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =       m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip    =      m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =    m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();

  if (m_firstEvent) {
    std::cout << " apv_evt = " << apv_evt << std::endl;

    std::cout << " size of apv_fec = " << apv_fec.size()
              << " fecNo[0] = " << apv_fec[0]  << std::endl;

    std::cout << " size of apv_id = " << apv_id.size()
              << " id[0] = " << apv_id[0]
              << " id[apv_id.size()-1] = " << apv_id[apv_id.size()-1]
              << std::endl;

    std::cout << " size of apv_ch = " << apv_ch.size()
              << " ch[0] = " << apv_ch[0]  << std::endl;

    std::cout << " size of mm_id = " << mm_id.size()
              << " mm_id[0] = " << mm_id[0]
              << " mm_id[mm_id.size()-1] = " << mm_id[mm_id.size()-1]  << std::endl;

    std::cout << " size of mm_readout = " << mm_readout.size()
              << " mm_readout[0] = " << mm_readout[0]
              << " mm_readout[mm_readout.size()-1] = " << mm_readout[mm_readout.size()-1]  << std::endl;

    std::cout << " size of mm_strip = " << mm_strip.size()
                << " mm_strip[0] = " << mm_strip[0]
                << " mm_strip[mm_strip.size()-1] = " << mm_strip[mm_strip.size()-1]  << std::endl;

    std::cout << " size of apv_qmax = " << apv_qmax.size()
              << std::endl;

    m_firstEvent = false;
  }

  int apvCh = 0;
  int timeBin = 0;
  int timeBinMax = 0;
  string currentChamber = m_chamberNames[0];

  map<string,int> minStripChamber;
  map<string,int> maxStripChamber;
  map<string,float> minChargeChamber;
  map<string,float> maxChargeChamber;

  minChargeChamber[currentChamber] = 3000.0;
  maxChargeChamber[currentChamber] = -100.0;

  timeBinMax = apv_q[1].size();
//  std::cout << " timeBinMax = " << timeBinMax << std::endl;
//  std::cout << " currentChamber = " << currentChamber << std::endl;

  for (auto row = apv_q.begin(); row != apv_q.end(); ++row)
  {
// one apvChannel (27 timebins)
    for (auto col = row->begin(); col != row->end(); ++col)
    {
//    std::cout << apvChannel << " " << timeBin << " " << *col << "  " ;
//    std::cout << "index = " << apvCh*timeBinMax + timeBin << " ";
//    std::cout << "mm_id = " << mm_id[apvCh]  <<  " ";

      if (mm_id[apvCh] != currentChamber) {
        currentChamber = mm_id[apvCh];
//        cout << " now chamber " << currentChamber << endl;
        minChargeChamber[currentChamber] = 3000.0;
        maxChargeChamber[currentChamber] = -100.0;
      }
      int stripNo = mm_strip[apvCh];
//    std::cout << "apvChannel = " << apvCh << " ";
//    std::cout << "index = " << (apvCh - apvChannelFirst)*timeBinMax + timeBin << " ";
//    std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
      if (*col > 3200 ) {
         std::cout << stripNo << " " << timeBin << " " << *col << "  " ;
      }

      if (*col > maxChargeChamber[currentChamber]) {
        maxChargeChamber[currentChamber] = *col;
        maxStripChamber[currentChamber] = mm_strip[apvCh];
//        cout << " maxChargeChamber " << " chamber = " << mm_id[apvCh] <<
//                " maxCharge = " <<  maxChargeChamber[currentChamber] <<
//                " maxStrip = " <<  maxStripChamber[currentChamber] <<  endl;
      }
         timeBin++;
    }
//       timeBinMax = timeBin;
       timeBin = 0;
       apvCh++;
  }

// the origin of the coordinate system is the centre of the first chamber
// strips are counting along the negative x-axis so away from the CR
// small strip counts correspond to positive x cooordinates

   float xRel[4];   // preliminary, vectorise
   float midStrip;
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     midStrip = (m_maxStrips[m_chamberNames[ii]]+1.0)/2.0;  // mid strip = 512.5
     xRel[ii] = (midStrip - float(maxStripChamber[m_chamberNames[ii]])) * m_pitch[m_chamberNames[ii]];
     m_xRel[ii]->Fill(xRel[ii]);
   }

  if (m_chamberNames.size() > 1) {
    for (int ii=1; ii<m_chamberNames.size(); ii++) {
      m_correlation[ii-1]->Fill(xRel[ii-1], xRel[ii]);
    }
  }

  return true;
}

void
MonitorBeam::Print(std::vector<unsigned int>& ev)
{
}

void
MonitorBeam::WriteToFile() {

  std::cout << " MonitorBeam::WriteToFile " << std::endl;

  m_outputFile->Write();
  m_outputFile->Close();
  std::cout << " MonitorChamber::WriteToFile " << " Closed file " << std::endl;

}

void
MonitorBeam::PublishHists() {

  for (int i=0; i<m_config->defined_chamber_names().size(); i++){
    m_histProvider->publish(*m_xRel[i],m_xRel[i]->GetName());
    if (i<m_config->defined_chamber_names().size()-1){
      m_histProvider->publish(*m_correlation[i], m_correlation[i]->GetName());
    }
  }
  

}

void MonitorBeam::getGeometry(void) {

// homebuilt way of getting geometry of chambers; see recomm
   float maxStrips;

   TEnv* tenv = (TEnv*)m_config->get_tenv_config();
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     stringstream ss;
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".ZPos";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     string str1 = tenv->GetValue(ss.str().c_str(), "");
//     cout << " str1= " << str1 << endl;
     m_zPos[m_chamberNames[ii]] = stof(str1);
     cout << "  chamber name = " << m_chamberNames[ii] << " zPos = "  << m_zPos[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Pitch";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     m_pitch[m_chamberNames[ii]] = stof(str1);
     cout << "  chamber name = " << m_chamberNames[ii] << " pitch = "  << m_pitch[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Max";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     maxStrips = stof(str1);
     cout << "  chamber name = " << m_chamberNames[ii] << " max strip = "  << maxStrips << endl;
     m_maxStrips[m_chamberNames[ii]] = maxStrips; // 1024
   }
}

int MonitorBeam::buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

   buildFrameContainer(rawEvent,m_datacontainerPtr );
//  std::cout << " buildEventVectors " << " size of data container = " << (*m_datacontainerPtr).size() << std::endl;

   err = m_decoder->buildEventContainer();
//  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

//  std::cout << " ProcessEvents " << " size of event container = " << (*m_eventcontainerPtr).size() << std::endl;

  err = m_eventVectors->buildVectors();

//  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;

}


void MonitorBeam::pedestalVectors() {

  const std::map<int, double>* pedestals_mean =  m_config->pedestal_mean_map();
  const std::map<int, double>* pedestals_stdev =  m_config->pedestal_stdev_map();
  std::cout << " MonitorBeam::pedestalVectors " << " size of pedestals_mean = " << pedestals_mean->size() << std::endl;

// vectorize the maps, indexed by 'sequential' channel number as apv vectors
  int ii = 0;
//  std::cout << " Means " << std::endl;
  for(auto it = pedestals_mean->begin(); it != pedestals_mean->end(); ++it, ii++ ) {
//    std::cout << it->first << " " << it->second << "\n";
    m_pedestal_mean.push_back(it->second);
  }

/********************************************************
  for (int jj = 0; jj<m_pedestal_mean.size(); jj++) {
  std::cout << " MonitorBeam::pedestalVectors " <<  " jj = " << jj
            << " m_pedestal_mean[jj] = "  << m_pedestal_mean[jj] << std::endl;
  }
*********************************************************/

  ii = 0;
//  std::cout << " Stdev s " << std::endl;
  for(auto it = pedestals_stdev->begin(); it != pedestals_stdev->end(); ++it, ii++ ) {
//    std::cout << it->first << " " << it->second << "\n";
    m_pedestal_stdev.push_back(it->second);
  }

/*************************************************
  for (int jj = 0; jj<m_pedestal_stdev.size(); jj++) {
  std::cout << " MonitorBeam::pedestalVectors " <<  " jj = " << jj
            << " m_pedestal_stdev[jj] = "  << m_pedestal_stdev[jj] << std::endl;
  }
****************************************************/

}


void MonitorBeam::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Beam_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorBeam::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();	// current directory

}
