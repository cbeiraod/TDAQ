/*
 mmTrack.h
 */

#ifndef mmTrack_h
#define mmTrack_h

#include <list>
#include <string>
#include <vector>

class CConfiguration;
class CEventDecoder;
class CMMEvent;
class CUDPData;
class CEventVectors;

   CConfiguration*	m_config;
   CEventDecoder*       m_decoder;
   CEventVectors*       m_eventVectors;

   std::list <CUDPData*>        m_datacontainer;
   std::list <CMMEvent*>        m_eventcontainer;

   CEvent::event_type_type      m_preset_event_type;
   CConfiguration::runtype_t    m_preset_run_type;

   time_t m_run_start_time;
   bool   m_save_data_flag;

#endif
