#include <string>
#include <utility>

class Cluster{

public:
  Cluster(std::string chamber, std::pair<int,int> strips, double pos_weighted_strip,
          int Qmax, int QmaxStripNo, int charge);
  virtual  ~Cluster();

public:
  double get_weighted_strip() const { return m_pos_weighted_strip;}
  int get_qMax() const { return m_qMax;}
  int get_charge() const { return m_charge;}
  int get_qMaxStripNo() const { return m_qMaxStripNo;}
  int get_width();

private:
  std::string m_chamber;
  std::pair<int,int> m_strips;  // first and last strip number
  double m_pos_weighted_strip;  // charge weighted pos
  int m_qMax;                // cluster qMax
  int m_qMaxStripNo;         // strip number of cluster qMax
  int m_charge;              // cluster charge

};
