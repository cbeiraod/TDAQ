#include "CUDPData.h"
#include "CConfiguration.h"
#include "CDetChamber.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"

#include <oh/OHRootProvider.h>

#include "MonitorQmaxRealTime.h"

using namespace std;

void buildEventVectors(std::vector<unsigned int>& rawEvent);
void buildFrameContainer(vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);

MonitorQmaxRealTime::MonitorQmaxRealTime(std::string name, CConfiguration* config, std::string chamberName)
{
  m_MonitorName = name;
  m_chamberName = chamberName;
  m_config = config;
  m_lastRecordTime = time(NULL);

  m_firstEvent = true;

// defining histograms
// this could also be done in the 'firstEvent' part of Process IF event parameters are mneeded to define the histos

  std::cout << " MonitorQmaxRealTime::MonitorQmaxRealTime " << " define histogram and graph " << std::endl;

  m_maxQ_event = new TH1F((m_chamberName + "_qmaxEventRealTime").c_str(),
                      (m_chamberName + " Event Qmax RealTime").c_str(),
                      400, 0, 4000.0); 
  m_maxQ_event->GetXaxis()->SetTitle("max strip charge of event");
  m_maxQ_event->SetCanExtend(TH1::kAllAxes);

  m_maxQ_time = new TGraph();
  TString chamberNameTstr = TString(m_chamberName);
  m_maxQ_time->SetTitle(chamberNameTstr + " Mean of Qmax vs Time; time(seconds); Mean of Qmax");
  m_point = 0;
}

MonitorQmaxRealTime::~MonitorQmaxRealTime()
{
}

bool
MonitorQmaxRealTime::Process(vector<unsigned int>& ev)
{

  cout<< " MonitorQmaxRealTime::Process " <<endl;
  buildEventVectors(ev);

  unsigned int apv_evt =                        m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =          m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =          m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =          m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id =            m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =       m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip    =      m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q =    m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =                m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =              m_eventVectors->get_apv_tbqmax();

  if (m_firstEvent) {
    std::cout << " apv_evt = " << apv_evt << std::endl;

    std::cout << " size of apv_fec = " << apv_fec.size()
              << " fecNo[0] = " << apv_fec[0]  << std::endl;

    std::cout << " size of apv_id = " << apv_id.size()
              << " id[0] = " << apv_id[0]
              << " id[1] = " << apv_id[1]
              << " id[127] = " << apv_id[127]
              << " id[129] = " << apv_id[129]  << std::endl;

    std::cout << " size of apv_ch = " << apv_ch.size()
              << " ch[0] = " << apv_ch[0]  << std::endl;

    std::cout << " size of mm_id = " << mm_id.size()
              << " mm_id[0] = " << mm_id[0]
              << " mm_id[257] = " << mm_id[257]  << std::endl;

    std::cout << " size of mm_readout = " << mm_readout.size()
              << " mm_readout[0] = " << mm_readout[0]
              << " mm_readout[257] = " << mm_readout[257]  << std::endl;

    std::cout << " size of mm_strip = " << mm_strip.size()
              << " mm_strip[0] = " << mm_strip[0]
              << " mm_strip[257] = " << mm_strip[257]  << std::endl;

    std::cout << " size of apv_qmax = " << apv_qmax.size()
              << " apv_qmax[0] = " << apv_qmax[0] << " apv_qmax[7] = " << apv_qmax[7]
              << std::endl;

    std::cout << " m_chamberName = " << m_chamberName << std::endl;

    m_firstEvent = false;
  }
 
  int maxQ_event = 0;   	// max Q of event
  int apvChannel = 0;

// the max calculation assumes pedestal corrected data !!
  for (auto row = apv_q.begin(); row != apv_q.end(); ++row, ++apvChannel)
  {
//    std::vector<int16_t>::const_iterator imax = max_element(row->begin(), row->end());  // Q max over timebins in one channel
    std::vector<int16_t>::iterator imax = max_element(row->begin(), row->end());	// Q max over timebins in one channel
//    std::cout << " apvChannel = " << apvChannel << " strip = " << mm_strip[apvChannel] << " Q max = " << *imax << std::endl;
    int tbQmax = std::distance(row->begin(),  imax);
//    std::cout << " tbQmax = " << tbQmax << std::endl;

    int maxQ = *imax;

    if(maxQ > maxQ_event) {
      maxQ_event = maxQ;
    }

  }
  m_maxQ_event->Fill(maxQ_event);

    sleep(0);   // TEST
  if (IsTimeToRecord()){
    double mean = m_maxQ_event->GetMean();
    std::cout << " Process " << "  mean of Qmax =  " << mean << " time = " <<
                time(NULL) << " m_lastRecordTime = " << m_lastRecordTime << std::endl;
    m_maxQ_time->SetPoint(m_point,time(NULL)-m_lastRecordTime, mean);
    m_point++;
    m_maxQ_event->Reset();
  }

 return true;
}

void
MonitorQmaxRealTime::Print(std::vector<unsigned int>& ev)
{
}

void
MonitorQmaxRealTime::WriteToFile() {

  std::cout << " MonitorQmaxRealTime::WriteToFile " << std::endl;

  m_maxQ_time->Write("QmaxVsTime");
  
  m_outputFile->Write();
  m_outputFile->Close();
  std::cout << " MonitorQmaxRealTime::WriteToFile " << " Closed file " << std::endl;
}

void
MonitorQmaxRealTime::PublishHists() {

  std::cout << " MonitorQmaxRealTime::PublishHists " << std::endl;
  m_histProvider->publish(*m_maxQ_time,"maxQ_RealTime");

}

void MonitorQmaxRealTime::buildEventVectors(std::vector<unsigned int>& rawEvent) {

  buildFrameContainer(rawEvent,m_datacontainerPtr );
  std::cout << " buildEventVectors " << " size of data container = " << (*m_datacontainerPtr).size() << std::endl;

  int err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

  std::cout << " ProcessEvents " << " size of event container = " << (*m_eventcontainerPtr).size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;
}

void MonitorQmaxRealTime::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Qmax_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorQmaxRealTime::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();	// current directory

}

bool MonitorQmaxRealTime::IsTimeToRecord()
{
  time_t currentTime = time(NULL);
  if( static_cast<long int>(currentTime-m_lastRecordTime) < m_qmaxMeanTime) {
    return false;
  }
//  m_lastRecordTime = time(NULL);
  return true;
}

