/**************************************************/
/*  dump raw APV data				  */
/*                                                */
/*  upgraded to APZ  Jan. 2018                    */
/*  author: J.Petersen  		          */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>

#include <TApplication.h>
#include "TCanvas.h"
#include "TH1I.h"
#include "TGraph.h"

#include "CUDPData.h"
#include "DAQEventReader.h"
#include "FileEventReader.h"

bool g_onlineFlag = true;
int  g_apvNo;

const int RODHDRSIZE = 9;
const int FRAMEHDRSIZE = 5;		// IP source, len, 3 SRS header words

using namespace std;

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);
void term_handler(int);

int 
main(int argc, char** argv)
{
  CmdArgStr partition_name ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt events         ('e', "events", "events-number", "number of events to retrieve (default 1)\n"
                                                         "-1 means work forever" );
  CmdArgStr datafilename   ('f', "datafilename", "asynchronous", "Data file name, if entered it reads events from the file instead of online stream");
  CmdArgInt apvNo          ('a', "apvnumber", "apv-number", "apv number to plot (0-15)");
   
  partition_name = getenv("TDAQ_PARTITION");
  events = 1;
  apvNo = -1;

  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &datafilename, &apvNo, NULL);
  cmd.description( "RCD APV graphical event dumper" );
  cmd.parse( arg_iter );

  g_apvNo = apvNo;

// this should be AFTER the command line parsing above
  TApplication *myapp = new TApplication("apvDump", &argc, argv);

  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  if (datafilename.isNULL()) {
   g_onlineFlag = true;
   ProcessEvents<DAQEventReader, IPCPartition>(partition, events);
  }
  else {
   g_onlineFlag = false;
   ProcessEvents<FileEventReader, string>(string(datafilename), events);
  }

  myapp->Run();

  return 0;
}

template <typename EventReader, typename Source>
void
ProcessEvents(Source src, int events) {

  EventReader evReader(src);
  const int eventsInFile = evReader.GetNumberOfEvents();  // doesn't work BL4S 2017/08/14 ??
  cout << "Number of events: " << events << endl;
  cout << "Number of events in file: " << eventsInFile << endl;
  cout << "g_onlineFlag: " << g_onlineFlag << endl;
  int eventCount = 0;

  std::vector <TGraph*> apvRaw;
  int noApvGraphs = 0;

  unsigned int apv_ptr = RODHDRSIZE + 0;

  while (eventCount < events || events == -1) {         // event loop
    cout << "\n # Event No.: " << eventCount << endl;

    vector<unsigned int> rawEvent = evReader.GetNextRawEvent();

// specific code for ONE event monitoring programs
    if ((g_onlineFlag == true) && (eventCount == 1)) break;
    if ((g_onlineFlag == false) && (eventCount < (events-1))) {
      std::cout << " skip event number " << eventCount << std::endl;
      ++eventCount;
      continue;
    }

    do {  // loop over APVs

      std::cout << " IP header = " << std::hex << rawEvent[apv_ptr] << std::dec << std::endl;
      int nWordsInAPV = rawEvent[apv_ptr+1];  // # words in APV after this word ...
      std::cout << " len of UDP APV frame = " << nWordsInAPV << std::dec << std::endl;
      int frameCounter = (rawEvent[apv_ptr+2] & 0xff000000) >> 24;
      std::cout << " frame counter = " << frameCounter << std::dec << std::endl;
      int apvNo = (rawEvent[apv_ptr+3] & 0xff000000) >> 24;
      std::cout << " APV number  = " << apvNo << std::endl;

      std::string ap(1,(char)((rawEvent[apv_ptr+3] & 0x00ff0000)>>16));
      std::cout << " ap type = " << ap << std::endl;

      TString apvNoTstr;
      apvNoTstr.Form("%d",apvNo);

      TGraph* apvGr = new TGraph();
      apvRaw.push_back(apvGr);

      if (ap == "Z" ){
        apvRaw[noApvGraphs]->SetTitle(" APZ data, APV number " + apvNoTstr +
                                      ";channel number * timebin; pulse height");
        // APV header
        int noSamples = (rawEvent[apv_ptr+5] & 0xff000000) >> 24;
        std::cout << " # samples = " << noSamples << std::endl;
        int noChannels = (rawEvent[apv_ptr+5] & 0x000000ff);
        std::cout << " # channels = " << noChannels << std::endl;

        if (noChannels > 0) {
        //transform data to words 16bit NB: rawEvent IS byte swapped but not word swapped, see also CApvEvent.cpp
        // start at first chan_info
          std::vector<int16_t> data16;
          for (int ii=0; ii<nWordsInAPV-5; ii++) {  // chan_info
            int16_t low16z = rawEvent[apv_ptr+7+ii] & 0xffff;
            int16_t high16z = (rawEvent[apv_ptr+7+ii] & 0xffff0000 ) >> 16;
//             std::cout << " low16z = " << std::hex << (unsigned short int)low16z << " high16z = " << (unsigned short int)high16z << std::endl;
            data16.push_back(low16z);
            data16.push_back(high16z);
          }
          // now process Channels above threshold
          int ll = 0;
          for (int jj= 0;jj<noChannels;jj++) {
            int chanNo = data16[jj*(noSamples+1)] & 0xff;    // chan_info header
//            std::cout << " channel number = " << std::dec << chanNo << std::endl;
            for (int kk=0; kk<noSamples;kk++) {
//              std::cout << " raw ph = " << std::hex << data16[jj*noSamples+kk+1] << std::endl;
              int16_t ph = -data16[jj*noSamples+kk+1];
//              std::cout << " ph = " << std::dec << ph << std::endl;
              apvRaw[noApvGraphs]->SetPoint(ll,ll,ph);
              ll++;
            }
            // insert a channel separator
              apvRaw[noApvGraphs]->SetPoint(ll,ll,100);
              ll++;
              apvRaw[noApvGraphs]->SetPoint(ll,ll,-100);
              ll++;
          }
        }
      }

      else {  // ADC mode
        apvRaw[noApvGraphs]->SetTitle(" APV data, channel number " + apvNoTstr + ";index;raw data");
        for (int ii=0; ii<nWordsInAPV-3; ii++) {
          int low16 = rawEvent[apv_ptr+FRAMEHDRSIZE+ii] & 0xffff;
          int high16 = (rawEvent[apv_ptr+FRAMEHDRSIZE+ii] & 0xffff0000 ) >> 16;
//          std::cout << " low16 = " << low16 << " high16 = " << high16 << std::endl;
          apvRaw[noApvGraphs]->SetPoint(2*ii,2*ii,low16);
          apvRaw[noApvGraphs]->SetPoint(2*ii+1,2*ii+1,high16);
        }
      }

      noApvGraphs++;
      apv_ptr += (nWordsInAPV + 2);

      std::cout << " rawEvent[apv_ptr+2] = " << std::hex << rawEvent[apv_ptr+2] <<  std::dec << std::endl;

    } while(rawEvent[apv_ptr+2] != SRS_NEXT_EVENT);

      
    TCanvas* canvas0 = new TCanvas("canvas0", "c0", 900, 700);
    if (g_apvNo >= 0) {   // ONE APV
      if (apvRaw[g_apvNo]->GetN() > 0) {
        apvRaw[g_apvNo]->Draw("");
      }
    }
    else {
      canvas0->Divide(2, (noApvGraphs+1)/2);
      for (int kk=0;kk<noApvGraphs;kk++) {
        canvas0->cd(kk+1);
        if (apvRaw[kk]->GetN() > 0) {
          apvRaw[kk]->Draw("");
        }
      }
    }

    canvas0->Update();

    ++eventCount;
  }
  cout << "\n" << endl;
}

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}


