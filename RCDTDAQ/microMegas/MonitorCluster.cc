/***********************

2018/01 

Cluster finding and coordinate computation
Tracking

The code is inspired by RD51, mmdaq recomm but is STRONGLY reduced in complexity
Notable differences:
. the only class defined is 'cluster'.
. no detectors, planes, readouts etc.
  Each chamber has just ONE readout

J.O.Petersen

**************************************/

#include <random>
#include <utility>

#include "CUDPData.h"
#include "CConfiguration.h"
#include "CDetChamber.h"
#include "CEventDecoder.h"
#include "CEventVectors.h"

#include <oh/OHRootProvider.h>

#include <TString.h>
#include "TCanvas.h"
#include "TGraph.h"
#include <TEnv.h>
#include <TFitResult.h>
#include <TStyle.h>
#include <TAxis.h>
#include "TF1.h"

#include "MonitorCluster.h"

using namespace std;

void buildFrameContainer(std::vector<unsigned int>& rawEvent, std::list <CUDPData*>* m_datacontainer);

MonitorCluster::MonitorCluster(std::string name, CConfiguration* config, std::string runNumberStr, bool publish)
{
  m_MonitorName = name;
  m_config = config;

  m_firstEvent = true;

// BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

  m_chamberNames = m_config->defined_chamber_names(); // rename ..

// number chambers
// 2018/03: not quite clear WHY order is like that - returned from m_config
// NOT the sequence along Z !!
// BL4Smm1 m_nameMap = 0, BL4Smm2 m_nameMap = 1, BL4Smm3 m_nameMap = 2, BL4Smm4 m_nameMap = 3
  int chNum = 0;
  for (auto chamberName: m_chamberNames){
    m_nameMap[chamberName]=chNum++;
    std::cout << " chamberName = " << chamberName << " m_nameMap = "
              << m_nameMap[chamberName] << std::endl;
  }

// all histograms are indexed by chamber numbers: 1,2,3,4 NOT necessarily being the chambers names.
  for (auto chamberName: m_chamberNames) {
    TH1* addHist(0);

    addHist = new TH1I((chamberName + "_cluster_width").c_str(),
                        (chamberName + " cluster width").c_str(),
                        20, 0, 20);
    addHist->GetXaxis()->SetTitle(" cluster width");
    m_TH1cluster_width.push_back((TH1I*)addHist);

    addHist = new TH1I((chamberName + "_number_clusters").c_str(),
                        (chamberName + " number of clusters per event").c_str(),
                        10, 0, 10);
    addHist->GetXaxis()->SetTitle(" # clusters");
    m_TH1number_clusters.push_back((TH1I*)addHist);

    addHist = new TH1I((chamberName + "_cluster_charge").c_str(),
                        (chamberName + " cluster charge").c_str(),
                        200, 0, 40000);
    addHist->GetXaxis()->SetTitle(" cluster charge");
    m_TH1cluster_charge.push_back((TH1I*)addHist);

    addHist = new TH1I((chamberName + "_cluster_qMax").c_str(),
                        (chamberName + " cluster qMax").c_str(),
                        200, 0, 2000);
    addHist->GetXaxis()->SetTitle(" qMax cluster");
    m_TH1cluster_qMax.push_back((TH1I*)addHist);

    addHist = new TH2I((chamberName + "_qMax_vs_width").c_str(),
                       (chamberName + " qMaxVsWidth").c_str(),
                       200,0,2000,10,0,10);
    addHist->GetXaxis()->SetTitle(" qmax cluster");
    addHist->GetYaxis()->SetTitle(" cluster width");
    m_TH2qMaxVsWidth.push_back((TH2I*)addHist);

    addHist = new TH2I((chamberName + "_qMax_vs_charge").c_str(),
                       (chamberName + " qMaxVsCharge").c_str(),
                       200,0,2000,200,0,40000);
    addHist->GetXaxis()->SetTitle(" qmax cluster");
    addHist->GetYaxis()->SetTitle(" cluster total charge");
    m_TH2qMaxVsCharge.push_back((TH2I*)addHist);

    addHist = new TH1F((chamberName + "_xyRelative").c_str(),
                      (chamberName + " xy coordinate").c_str(),
                      1000, -250.0, 250.0);    // mm
    addHist->GetXaxis()->SetTitle(" coordinate relative to middle of chamber (mm)");
    addHist->GetYaxis()->SetTitle(" events");
    m_TH1xyRel.push_back((TH1F*)addHist);

    addHist = new TH1F((chamberName + "_xyRelativeStripNo").c_str(),
                      (chamberName + " xy coordinate strip number").c_str(),
                      1000, -250.0, 250.0);    // mm
    addHist->GetXaxis()->SetTitle(" coordinate relative to middle of chamber (mm)");
    addHist->GetYaxis()->SetTitle(" events");
    m_TH1xyRelStripNo.push_back((TH1F*)addHist);

    addHist = new TH1F((chamberName + "_weightedMinusStrip").c_str(),
                      (chamberName + " weighted minus strip number").c_str(),
                      100, -2.0, 2.0);    // mm
    addHist->GetXaxis()->SetTitle(" weighted coordinate minus strip number coordinate (mm)");
    addHist->GetYaxis()->SetTitle(" events");
    m_TH1weightedMinusStrip.push_back((TH1F*)addHist);

    addHist = new TH1F((chamberName + "_resolution").c_str(),
                      (chamberName + " fit - measured").c_str(),
                      200,-10.0,10.0);
    addHist->GetXaxis()->SetTitle(" mm");
    addHist->GetYaxis()->SetTitle(" events");
    m_resolution.push_back((TH1F*)addHist);

    addHist = new TH1F((chamberName + "_resolutionStripNo").c_str(),
                      (chamberName + " fit - measured (strip number)").c_str(),
                      200,-10.0,10.0);
    addHist->GetXaxis()->SetTitle(" mm");
    addHist->GetYaxis()->SetTitle(" events");
    m_resolutionStripNo.push_back((TH1F*)addHist);
    
  }

  m_TH1number_emptyEvents = new TH1I("number_empty_events", " number of empty events",
                                     10, 0, 10);                                     
  m_TH1number_emptyEvents->GetXaxis()->SetTitle(" # empty events");

  m_grTrack = new TGraph();
  m_grTrackStripNo = new TGraph();

// get some geometry parameters
  getGeometry();
 
}

MonitorCluster::~MonitorCluster()
{
}

bool
MonitorCluster::Process(vector<unsigned int>& ev)
{
// all APV fragments empty - cannot build vectors
  int retBuild = buildEventVectors(ev);
  std::cout << " retBuild = " << retBuild << std::endl;

  if( retBuild != CEventVectors::RETURN_OK) {
    m_TH1number_emptyEvents->Fill(1);
    return true;
  }

  unsigned int apv_evt =                     m_eventVectors->get_apv_evt();
  std::vector <unsigned int> apv_fec =       m_eventVectors->get_apv_fec();
  std::vector <unsigned int> apv_id  =       m_eventVectors->get_apv_id();
  std::vector <unsigned int> apv_ch  =       m_eventVectors->get_apv_ch();
  std::vector <std::string>  mm_id =         m_eventVectors->get_mm_id();
  std::vector <unsigned int> mm_readout =    m_eventVectors->get_mm_readout();
  std::vector <unsigned int> mm_strip    =   m_eventVectors->get_mm_strip();
  std::vector <std::vector <short> > apv_q = m_eventVectors->get_apv_q();
  std::vector <short> apv_qmax =             m_eventVectors->get_apv_qmax();
  std::vector <short> apv_tbqmax =           m_eventVectors->get_apv_tbqmax();

  if (m_firstEvent) {

    std::cout << " apv_evt = " << apv_evt << std::endl;

    std::cout << " size of apv_fec = " << apv_fec.size()
              << " fecNo[0] = " << apv_fec[0]  << std::endl;

    std::cout << " size of apv_id = " << apv_id.size()
              << " id[0] = " << apv_id[0]
              << " id[apv_id.size()-1] = " << apv_id[apv_id.size()-1]
              << std::endl;

    std::cout << " size of apv_ch = " << apv_ch.size()
              << " ch[0] = " << apv_ch[0]  << std::endl;

    std::cout << " size of mm_id = " << mm_id.size()
              << " mm_id[0] = " << mm_id[0]
              << " mm_id[mm_id.size()-1] = " << mm_id[mm_id.size()-1]  << std::endl;

    std::cout << " size of mm_readout = " << mm_readout.size()
              << " mm_readout[0] = " << mm_readout[0]
              << " mm_readout[mm_readout.size()-1] = " << mm_readout[mm_readout.size()-1]  << std::endl;

    std::cout << " size of mm_strip = " << mm_strip.size()
                << " mm_strip[0] = " << mm_strip[0]
                << " mm_strip[mm_strip.size()-1] = " << mm_strip[mm_strip.size()-1]  << std::endl;

    std::cout << " size of apv_qmax = " << apv_qmax.size()
              << std::endl;

    m_firstEvent = false;
  }
    std::cout << " size of mm_id = " << mm_id.size() << std::endl;

// for EACH event, construct strip to channel maps per chamber
  std::vector< std::map<int,int> > stripToChannel;  // ALL chanbers, indexed 0,1,2,3
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    std::map<int,int> stripToChannelChamber;
    for (int jj = 0; jj<mm_strip.size(); jj++) {  // jj = apvChannel 
//      std::cout << " jj = " << jj << " name from data = " <<  mm_id[jj]
//                << " chamber Name = " << m_chamberNames[ii] << std::endl;
      if (mm_id[jj] == m_chamberNames[ii]) {  // may not occur, if no hits in chamber
        int strip = mm_strip[jj];
//        std::cout << " apvChannel = "  << jj << " strip = " << strip << std::endl;
        stripToChannelChamber[strip] =  jj;
      }
    }
    stripToChannel.push_back(stripToChannelChamber);  // stripToChannelChamber may be empty ..
  }

  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    std::cout << " chamber name " << m_chamberNames[ii]
              << " size of  stripToChannelChamber = "
              << stripToChannel[ii].size() << std::endl;
  }

  std::vector <std::vector<std::pair<int,int> > > cluster_ranges; // ALL chambers, indexed by chamber number 0,1,2,3
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    std::vector<std::pair<int,int> > l_cluster_ranges;    // one chamber
    if (stripToChannel[ii].size() > 1) {  // more than ONE strip
      l_cluster_ranges = findClusterRanges(m_chamberNames[ii], apv_q, stripToChannel); // for a named chamber
    }
    std::cout << " Process " << " chamber name " << m_chamberNames[ii]
              << " size of l_cluster_ranges = " << l_cluster_ranges.size() << std::endl;
    m_TH1number_clusters[ii]->Fill(l_cluster_ranges.size());
    cluster_ranges.push_back(l_cluster_ranges); // cluster_ranges may be empty
  }
  std::cout << " size of cluster_ranges = " << cluster_ranges.size() << std::endl;

// make clusters in a chamber 
  std::vector <std::vector<Cluster*> > clusters; // ALL chambers, indexed by chamber number 0,1,2,3
//  std::vector<Cluster*> cluInit(0,0); // empty vector
//  clusters.push_back(cluInit);  // init m_cluster to allow push_back
  std::cout << " Process " << " size of clusters = " <<  clusters.size() << std::endl;
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    std::vector<Cluster*> l_clusters;  // one chamber
    l_clusters = makeClusters(m_chamberNames[ii], apv_q, cluster_ranges[ii],stripToChannel); // for named chamber
     clusters.push_back(l_clusters);
  }
  std::cout << " Process " << " size of clusters = " << clusters.size() << std::endl;

  std::vector <std::vector<double> > xyRel; // for all chambers in event, indexxed bychamber number 0,1,2,3
  std::vector <std::vector<double> > xyRelStripNo; // based on strip number
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    std::vector<double> w_xyRel;  // one chamber, weighted ..
    float noGaps = m_maxStrips[m_chamberNames[ii]] - 1.0;
// length = from middle of first strip to middle of last strip
    double xyLength = noGaps * m_pitch[m_chamberNames[ii]];
    std::cout << " chamber name = " << m_chamberNames[ii]
              << " xyLength = " << xyLength
              << " clusters size = " << clusters[ii].size() << std::endl;

    for (int jj=0; jj<clusters[ii].size(); jj++) {  // clusters in chamber, could be zero ..
      m_TH1cluster_qMax[ii]->Fill(clusters[ii][jj]->get_qMax());
      double weighted_strip_no = clusters[ii][jj]->get_weighted_strip();
// strip #1 should correspond to xyRel = - xyLength/2.0 - strips start at 1
      double d_xyRel = (weighted_strip_no -1) * m_pitch[m_chamberNames[ii]] - xyLength/2.0;
      std::cout << " chamber name = " << m_chamberNames[ii]
                << " weighted_strip_no = " <<  weighted_strip_no
                << " d_xyRel = " << d_xyRel << std::endl;
      m_TH1xyRel[ii]->Fill(d_xyRel);
      w_xyRel.push_back(d_xyRel);
    }
    xyRel.push_back(w_xyRel);

// same for strip number coordinates
    std::vector<double> s_xyRel;  // one chamber
    for (int jj=0; jj<clusters[ii].size(); jj++) {  // clusters in chamber, could be zero ..
      double qMax_strip_no = clusters[ii][jj]->get_qMaxStripNo();
// strip #1 should correspond to xyRel = - xyLength/2.0 - strips start at 1
      double d_xyRel = (qMax_strip_no -1) * m_pitch[m_chamberNames[ii]] - xyLength/2.0;
      std::cout << " chamber name = " << m_chamberNames[ii]
                << " qMax_strip_no = " <<  qMax_strip_no
                << " d_xyRel = " << d_xyRel << std::endl;
      m_TH1xyRelStripNo[ii]->Fill(d_xyRel);
      s_xyRel.push_back(d_xyRel);
    }
    xyRelStripNo.push_back(s_xyRel);

// histogram difference between weighted and strip coodinates
    for (int jj=0; jj<clusters[ii].size(); jj++) {  // clusters in chamber, could be zero ..
    m_TH1weightedMinusStrip[ii]->Fill(xyRel[ii][jj] -  xyRelStripNo[ii][jj]);
    }

  }

  int no_good_chambers = 0;  //  with #clusters =1 ...
  int no_chambers_90degrees = 0;
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
   std::cout << " chamber name = " << m_chamberNames[ii] << 
                " # clusters = " << clusters[ii].size() << std::endl;
   if (clusters[ii].size() == 1) no_good_chambers++;
   if (m_angle[m_chamberNames[ii]] == 90.0) no_chambers_90degrees++;    //FP comparison?
  }
  std::cout << " no_chambers_90degrees = " << no_chambers_90degrees << std::endl;


  if (no_good_chambers == m_chamberNames.size()) {
    if (no_chambers_90degrees == m_chamberNames.size()) { // strips in all chambers aligned
      fitAllX(xyRel,xyRelStripNo);
    }
  }

  // delete clusters (new)
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    std::cout << " delete, clusters[ii].size() = " << clusters[ii].size() << std::endl;
    if (clusters[ii].size() > 0) {
      for (int jj=0; jj<clusters[ii].size(); jj++) {
        delete clusters[ii][jj];
        std::cout << " deleted cluster = " << clusters[ii][jj] << std::endl;
      }
    }
  }


  return true;
}

void
MonitorCluster::Print(std::vector<unsigned int>& ev)
{
}

void
MonitorCluster::WriteToFile() {

  std::cout << " MonitorCluster::WriteToFile " << std::endl;

  m_outputFile->Write();
  m_outputFile->Close();
  std::cout << " MonitorChamber::WriteToFile " << " Closed file " << std::endl;

}

void
MonitorCluster::PublishHists() {
  for (int i=0; i<m_config->defined_chamber_names().size(); i++){
    m_histProvider->publish(*m_TH1cluster_width[i],m_TH1cluster_width[i]->GetName());
    m_histProvider->publish(*m_TH1number_clusters[i],m_TH1number_clusters[i]->GetName());
    m_histProvider->publish(*m_TH1xyRel[i],m_TH1xyRel[i]->GetName());
    m_histProvider->publish(*m_TH1xyRelStripNo[i],m_TH1xyRelStripNo[i]->GetName());
    m_histProvider->publish(*m_TH1cluster_charge[i],m_TH1cluster_charge[i]->GetName());
    m_histProvider->publish(*m_TH1cluster_qMax[i],m_TH1cluster_qMax[i]->GetName());
    m_histProvider->publish(*m_TH2qMaxVsWidth[i],m_TH2qMaxVsWidth[i]->GetName());
    m_histProvider->publish(*m_TH2qMaxVsCharge[i],m_TH2qMaxVsCharge[i]->GetName());
    m_histProvider->publish(*m_TH1weightedMinusStrip[i],m_TH1weightedMinusStrip[i]->GetName());
    m_histProvider->publish(*m_resolution[i],m_resolution[i]->GetName());
    m_histProvider->publish(*m_resolutionStripNo[i],m_resolutionStripNo[i]->GetName());

  }
  m_histProvider->publish(*m_TH1number_emptyEvents,m_TH1number_emptyEvents->GetName());
}

// input: chamber name and apv_q and stripToChannel structures for ALL chambers
// stripToChannel is a vector of maps, each possibly empty
// algorithm:
// assume at leat two strips in chamber
// a cluster range is a vector of strip ranges (pairs)
// initialise a strip range with values of first strip
// loop over following strips strips in chamber
// if strips found, that ARE close in space and time, update the strip range
// if a strip is 'far' in space and time, add current strip range to cluster range
// IF there are at least two strips in range
// after last strip, a cluster may be pending, very likely e.g if only one 'clean' cluster in chamber
// IF there is a strip range (two strips) 'pending' add the strip range to cluster

// NOTE. Cluster range only defined if at least TWO strips in cluster
// this eliminates one strip clusters     REVIEW

std::vector<std::pair<int,int> > MonitorCluster::findClusterRanges(string chName,
                                  std::vector <std::vector <short> >& apv_q,
                                  std::vector< std::map<int,int> >& stripToChannel) {

    std::vector<std::pair<int,int> > clusterRanges;
    int currentId = m_nameMap[chName];  // get the index back ..
//  for (std::map<int,int>::iterator it = stripToChannel[currentId].begin(); it != stripToChannel[currentId].end(); it++) {
//    std::cout << " findClusterRange " << " strip = " << it->first << " channel = " << it->second << std::endl;
//  }

    if (stripToChannel[currentId].size() == 0) {   // no strips
      return clusterRanges;
    }

    std::map<int,int>::iterator ifst = stripToChannel[currentId].begin();
    int prev_strip_no =  ifst->first;
    int prev_chan =  ifst->second;
    std::cout << " findClusterRange " << " chamber name = " << chName
              << " current ID = " << currentId
              << " prev_strip_no = " << prev_strip_no
              << " prev_chan = " << prev_chan << std::endl;

    std::vector<int16_t>::iterator first_imax = max_element(apv_q[prev_chan].begin(), apv_q[prev_chan].end());
    int prev_strip_maxTimeBin = std::distance(apv_q[prev_chan].begin(), first_imax);
    std::cout << " findClusterRange " << " chamber name = " << chName
              << " prev_strip_maxTimeBin = "
              << prev_strip_maxTimeBin << std::endl;

    std::pair <int,int> strip_range;
    strip_range.first = prev_strip_no;
    strip_range.second = prev_strip_no;

    std::map<int,int>::iterator istrip = stripToChannel[currentId].begin();

    std::cout << " findClusterRange " << " istrip, first = " << istrip->first << " istrip, second = " << istrip->second << std::endl;

    istrip++;  // skip first strip 
    std::cout << " findClusterRange " << " istrip, first = " << istrip->first << " istrip, second = " << istrip->second << std::endl;

    for (std::map<int,int>::iterator im = istrip; im != stripToChannel[currentId].end(); im++) {
      int curr_stripNo = im->first;
      int curr_chanNo = im->second;
      std::vector<int16_t>::iterator imax = max_element(apv_q[curr_chanNo].begin(), apv_q[curr_chanNo].end());
      int curr_strip_maxTimeBin = std::distance(apv_q[curr_chanNo].begin(), imax);
      std::cout << " findClusterRange " << " chamber name = " << chName
                << " curr_stripNo = " << curr_stripNo
                << " curr_strip_maxTimeBin = " << curr_strip_maxTimeBin << std::endl;
      int stripdiff = curr_stripNo - prev_strip_no;
      int timeBindiff = curr_strip_maxTimeBin - prev_strip_maxTimeBin;
      std::cout << " findClusterRange " << " chamber name = " << chName
                << " stripdiff = " << stripdiff
                << " timeBindiff = " << timeBindiff << std::endl;

      if (stripdiff <= CreateClustersMaxGap && std::abs(timeBindiff) <= CreateClustersMaxTimeBinGap) {
       strip_range.second = curr_stripNo;
      }
      else {
        if ((strip_range.second - strip_range.first) >= 1) {  // at least two strips ..
          std::cout << " findClusterRange " << " chamber name = " << chName << " push cluster " << std::endl;
          clusterRanges.push_back(strip_range);
        }
      // search for next cluster
        strip_range.first = curr_stripNo;
        strip_range.second = curr_stripNo;
      }
      prev_strip_no = curr_stripNo;
      prev_strip_maxTimeBin = curr_strip_maxTimeBin;
    }

  // pending strip range, quite probable ..
    if ((strip_range.second - strip_range.first) >= 1) {  // at least two strips ..
      std::cout << " findClusterRange " << " chamber name = " << chName
                << " push cluster last " << " # strips = "
                << (strip_range.second - strip_range.first + 1) << std::endl;
      clusterRanges.push_back(strip_range);
    }

    std::cout << " findClusterRange " << " chamber name = " << chName
              << " clusterRanges size = " << clusterRanges.size() << std::endl;
    for (int ii = 0; ii<clusterRanges.size(); ii++) {
      std::cout << " first = " << clusterRanges[ii].first 
                << " second = " << clusterRanges[ii].second << std::endl;
    }

    return clusterRanges;
  }

std::vector<Cluster* > MonitorCluster::makeClusters(std::string chName,
                                  std::vector <std::vector <short> >& apv_q,
                                  std::vector<std::pair<int,int> >& clusterRanges,
                                  std::vector< std::map<int,int> >& stripToChannel) {

  std::cout << " MonitorCluster::makeClusters " << " clusterRanges.size() = " << clusterRanges.size() << std::endl;

  int currentId = m_nameMap[chName];

  std::vector<Cluster*> my_clusters;  // one chamber

  if (clusterRanges.size() > 0) {
    for (int jj=0; jj<clusterRanges.size(); jj++) {
      int strip1 = clusterRanges[jj].first;
      int strip2 = clusterRanges[jj].second;
      std::cout << " strip = " << strip1 << " strip2 = " << strip2 << std::endl;

// variables for a CLUSTER:
// qStrip          SUM of charges over timebins for ONE strip
// qMaxStrip       max charge of a strip (over timebins)
// qMaxCluster     max charge of cluster
// qMaxStripNo     strip number of max charge of cluster
// qStripSum       SUM of strip charges i.e. the charge of the cluster
// stripQmaxSum    SUM of (strip number*qMaxStrip) over strips in cluster
// qMaxStripSum    SUM of the qMaxStrip. Note: the qMax is NOT in the same timebin for the strips
// weightedStripNo  = stripQmaxSum/qMaxStripSum

//  the cluster MAY CONTAIN a strip which is NOT in the stripToChannel map
// since cluster finding allows holes !!
      int qStrip = 0;
      int qMaxCluster = 0;
      int qMaxStripNo = 0;
      double stripQmaxSum = 0.0;
      int qStripSum = 0;
      double qMaxStripSum = 0.0;
      for (int is = strip1; is <= strip2; is++) {
        std::map<int,int>::iterator istoch;
        istoch = stripToChannel[currentId].find(is); 
        if (istoch != stripToChannel[currentId].end()) {  // strip in map
          int chanNo = stripToChannel[currentId][is];
          qStrip = std::accumulate(apv_q[chanNo].begin(), apv_q[chanNo].end(),0);
          std::vector<int16_t>::iterator imax = max_element(apv_q[chanNo].begin(), apv_q[chanNo].end());
          if (*imax > qMaxCluster) {
            qMaxCluster = *imax;
            qMaxStripNo = is;
          }
          std::cout << " makeClusters " << " chamber name = " << chName << " strip number = "
                    << is << " channel number = " << chanNo
                    << " qStrip = " << qStrip << " QMax = " << *imax
                    << " qMaxCluster = " << qMaxCluster  << " qMaxStripNo = " << qMaxStripNo
                    << " qStripSum = " << qStripSum
                    << " apv_q[chanNo], size = " << apv_q[chanNo].size() << std::endl;
          double qMaxStrip = (double)(*imax);
          qStripSum += qStrip;
          stripQmaxSum += ((double)is * (double)qMaxStrip);
          qMaxStripSum += (double)qMaxStrip;
        }
      }
      
      double weightedStripNo = stripQmaxSum / qMaxStripSum;

      Cluster* clu = new Cluster(chName,
                                 clusterRanges[jj],
                                 weightedStripNo,
                                 qMaxCluster,
                                 qMaxStripNo,
                                 qStripSum);
      std::cout << " makeClusters " << " clu = " << clu << std::endl;
      my_clusters.push_back(clu);
      std::cout << " makeClusters " << " size of my_clusters = " << my_clusters.size() << " currentId = " << currentId <<  std::endl;

      m_TH1cluster_width[currentId]->Fill(clu->get_width());
      m_TH1cluster_charge[currentId]->Fill(clu->get_charge());
//      m_TH1cluster_qMax[currentId]->Fill(clu->get_qMax());
      m_TH2qMaxVsWidth[currentId]->Fill(clu->get_qMax(),clu->get_width());
      m_TH2qMaxVsCharge[currentId]->Fill(clu->get_qMax(),clu->get_charge());
    }
  }
  else {
    std::cout << " makeClusters " << " size of my_clusters = " << my_clusters.size() << " currentId = " << currentId <<  std::endl;
    std::cout << " makeClusters end " << " no clusters .. " << std::endl;
  }

  return my_clusters;
   
}

Cluster::Cluster(string chamber, std::pair<int,int> strips, double pos_weighted_strip,
                 int qMax, int qMaxTripNo, int charge)
  : m_chamber(chamber), m_strips(strips), m_pos_weighted_strip( pos_weighted_strip),
    m_qMax(qMax), m_qMaxStripNo(qMaxTripNo), m_charge(charge)
{
};

int Cluster::get_width() {
  int strip1 = m_strips.first;
  int strip2 = m_strips.second;
  return (strip2 - strip1 +1);
}

Cluster::~Cluster() {
};

void MonitorCluster::fitAllX(std::vector <std::vector<double> >& xyRel,
                             std::vector <std::vector<double> >& xyRelStripNo) {

  std::cout << " one cluster per chamber " << std::endl;

  m_grTrack->Set(0);  // clear graph

  for (int ii=0; ii<m_chamberNames.size(); ii++) {
     m_grTrack->SetPoint(ii,m_zPos[m_chamberNames[ii]],xyRel[ii][0]);
     std::cout << " chamber = " << m_chamberNames[ii]
              << " zPos = " << m_zPos[m_chamberNames[ii]]
               << " x Pos = " << xyRel[ii][0] << std::endl;
  }
  TFitResultPtr r = m_grTrack->Fit("pol1","QS");
  cout << " intercept = " << r->Value(0) << " slope = " << r->Value(1) << endl;
//  r->Print("V");     // print full information of fit including covariance matrix

    
  float xRel,xFit;
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    xFit = r->Value(1)*m_zPos[m_chamberNames[ii]] + r->Value(0);
    xRel = xyRel[m_nameMap[m_chamberNames[ii]]][0];
    std::cout << " chamber = " << m_chamberNames[ii]
              << " xFit = " << xFit << " xRel = " << xRel << std::endl;
    m_resolution[ii]->Fill(xFit - xRel);
  }

// now, same calculation with with coordinates based on strip numbers
  m_grTrackStripNo->Set(0);  // clear graph

  for (int ii=0; ii<m_chamberNames.size(); ii++) {
     m_grTrackStripNo->SetPoint(ii,m_zPos[m_chamberNames[ii]],xyRelStripNo[ii][0]);
     std::cout << " chamber = " << m_chamberNames[ii]
              << " zPos = " << m_zPos[m_chamberNames[ii]]
              << " x Pos = " << xyRelStripNo[ii][0] << std::endl;
  }
  r = m_grTrackStripNo->Fit("pol1","QS");
  cout << " intercept = " << r->Value(0) << " slope = " << r->Value(1) << endl;
//  r->Print("V");     // print full information of fit including covariance matrix
 
  for (int ii=0; ii<m_chamberNames.size(); ii++) {
    xFit = r->Value(1)*m_zPos[m_chamberNames[ii]] + r->Value(0);
    xRel = xyRelStripNo[m_nameMap[m_chamberNames[ii]]][0];
    std::cout << " chamber = " << m_chamberNames[ii]
              << " strip number based " << " xFit = " << xFit << " xRel = " << xRel << std::endl;
    m_resolutionStripNo[ii]->Fill(xFit - xRel);
  }
}

void MonitorCluster::getGeometry(void) {

// homebuilt way of getting geometry of chambers; see recomm
   float maxStrips;

   TEnv* tenv = (TEnv*)m_config->get_tenv_config();
   for (int ii = 0; ii<m_chamberNames.size(); ii++) {
     stringstream ss;
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".ZPos";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     string str1 = tenv->GetValue(ss.str().c_str(), "");
//     cout << " str1= " << str1 << endl;
     m_zPos[m_chamberNames[ii]] = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " zPos = "  << m_zPos[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Angle";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     m_angle[m_chamberNames[ii]] = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " angle = "  << m_angle[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Pitch";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     m_pitch[m_chamberNames[ii]] = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " pitch = "  << m_pitch[m_chamberNames[ii]] << endl;

     ss.str("");
     ss << "mmdaq.Chamber." << m_chamberNames[ii] << ".Strips.X.Max";
//     cout << " ss = " << ss.str() << endl;
//     cout << " ss.str().c_str() = " << ss.str().c_str() << endl;
     str1 = tenv->GetValue(ss.str().c_str(), "");
     maxStrips = stof(str1);
     cout << " getGeometry " << "  chamber name = " << m_chamberNames[ii]
          << " max strip = "  << maxStrips << endl;
     m_maxStrips[m_chamberNames[ii]] = maxStrips; // 1024
   }
}

int MonitorCluster::buildEventVectors(std::vector<unsigned int>& rawEvent) {

  int err = 0;

std::cout << " buildEventVectors " << " size of rawEvent = " << rawEvent.size() << std::endl;

  buildFrameContainer(rawEvent,m_datacontainerPtr );
  std::cout << " buildEventVectors " << " size of data container = " << (*m_datacontainerPtr).size() << std::endl;

  err = m_decoder->buildEventContainer();
  std::cout << " ProcessEvents" << " after buildEventContainer, err = " << err << std::endl;

//  std::cout << " ProcessEvents " << " size of event container = " << (*m_eventcontainerPtr).size() << std::endl;

  err = m_eventVectors->buildVectors();

  std::cout << " ProcessEvents" << " after buildVectors(), err = " << err << std::endl;

  return err;

}

void MonitorCluster::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Cluster_monitor";
  string output_name;

  output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorCluster::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();	// current directory

}
