#include <stdexcept>

#include "RCDRawEvent.h"
#include "v1290.h"
#include "v792.h"
#include "v560.h"
#include "eudaq.h"
//#include "BL4SException.h"

#include<iostream>
#include<iomanip>
#include<bitset>

using namespace std;

RCDRawEvent::RCDRawEvent(vector<u_int> & vec_rawev) :
   vec_raw(vec_rawev), m_valid(false)
{
  event_size = vec_raw.size();
  m_iterator = vec_raw.begin();

  ReadEventHeader();
  if (!m_valid){
    cerr << "Malformed event header. Skipping!" << endl;
    return;
  }

  while(m_iterator != vec_raw.end()) {
    //u_int header = FindModuleHeader();
    bool moduleFound = FindAndReadModule();
    //if (! header)
    if(! moduleFound)
      break;
    //cout << std::hex << "header: " << header << endl;
    //ReadModuleData(header);
  }
}
 
RCDRawEvent::~RCDRawEvent() 
{
}

void
RCDRawEvent::ReadEventHeader()
{
  if (*m_iterator != 0xee1234ee){
    cout << "Event starts with 0x" << hex << *m_iterator << endl;
    m_valid = false;
    return;
  }
  const int nWords = *(++m_iterator);
  constexpr int nWordsExp = 9;
  if ( nWords != nWordsExp ){
    m_valid = false;
    cout << "Event header size=" << nWords << " (expected: "
         << nWordsExp << ")" << endl;
    return;
  }

  //TODO: Parse header
  m_event_num = *(m_iterator+4);

  m_iterator = m_iterator+(nWords-2);
  m_valid = true;
}

bool
RCDRawEvent::FindAndReadModule()
{
  // m_iterator is the m_iteratorerator
  m_iterator = find_if(m_iterator, vec_raw.end(), IsModuleHeader);

  // TODO what happens if it can't find?, returns last
  if (m_iterator == vec_raw.end()) {
    //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " End of event reached." << endl;
      return false;
  }
  //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " " << " " << std::hex << "0x" << *m_iterator << std::dec << " -> Module Header Found! " <<  endl;
  //std::cout << "RCDRawEvent::FindModuleHader: "<< " " << " " << std::hex << "0x" << *m_iterator << std::dec << std::endl;
  auto itType = m_iterator;
  ++m_iterator;
  switch(*itType) {
    case CAEN_TDC_v1x90x:
      Readv1290();
      return true;
      break;
    case CAEN_QDC_v792x:
      Readv792();
      return true;
      break;
    case CAEN_SCALER_v560x:
      Readv560();
      return true;
      break;
    case EUDAQ_MIMOSA:
      ReadMimosa();
      return true;
      break;
  }
}

/*
u_int 
RCDRawEvent::FindModuleHeader()
{
  m_iterator = find_if(m_iterator, vec_raw.end(), IsModuleHeader);

  // TODO what happens if it can't find?, returns last
  if (m_iterator == vec_raw.end()) {
    //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " End of event reached." << endl;
      return 0;
  }
  //debug->out(4) << "RCDRawEvent::FindModuleHader: "<< " " << " " << std::hex << "0x" << *m_iterator << std::dec << " -> Module Header Found! " <<  endl;
  return *m_iterator++;
}
*/ 

void 
RCDRawEvent::ReadModuleData(u_int ModuleID)
{
  if (m_iterator == vec_raw.end()) {
    //debug->out(4) << "RCDRawEvent::ReadModuleData: "<< " End of event reached." << endl;
    return;
  }
 
  switch (ModuleID) {
    case V792_ModuleID0:
      cout << "V792_ModuleID0" << endl;
      Readv792();
      break;
 
    case V792_ModuleID1:
      cout << "V792_ModuleID1" << endl;
      Readv792();
      break;
 
    case V1290_ModuleID0:
      Readv1290();
      break;
      
    case V1290_ModuleID1:
      Readv1290();
      break;
 
    case V560_ModuleID:
      Readv560();
      break;

    default :
      break;
  }
}
 

void 
RCDRawEvent::Readv792()
{
  u_int num, loop, ch_no, data;
  V792Data module;
  module.Channel.resize(32);

  //cout << "RCDRawEvent::Readv792: Reading module data" <<  endl;  

  num = *m_iterator++;
  if (num == 0)
    return;

  if (num < 2 || num > 34)  {
    cout << "Bad v792 fragment length: " << to_string(num) << " words" << endl;
    return;
    //throw BL4SRuntimeError({"Bad v792 fragment length: ", to_string(num), " words"});
  }
 
  //debug->out(5) << "RCDRawEvent::Readv792: Number of Words in 792 data: " << num <<  endl;  
 
  //TODO check if there is a header.
  data = *m_iterator++;
  module.qdc_header = data;
  //debug->out(5) << "RCDRawEvent::Read792: QDC Header: " << std::hex << data << std::dec << endl;

  //TODO: If not header, throw exception
  //TODO for safety there may be a check that if num>34, throw exception.
  for (loop = 0; loop < num-2; loop++) {
    data = *m_iterator++;
 
    //TODO check if it is an event or not.
    ch_no = (data >> 16) & 0x3f;
    if(ch_no >= num-2)
      return;
    module.Channel[ch_no].UF = (bool)((data >> 13) & 0x1); // Underthreshold
    module.Channel[ch_no].OF = (bool)((data >> 12) & 0x1); // Overflow
    module.Channel[ch_no].Data = data & 0xfff;             // Measurement
    module.Channel[ch_no].Valid = (bool)(((data >> 24) & 0x7)==0x000); // TODO valid if 000
    //debug->out(5) << "RCDRawEvent::Read792: QDC Word: (ch = " << ch_no << ") " <<  std::hex << data << std::dec << ", measurement: " << (data & 0xfff)
    //<< ", Underthreshold: " <<module.Channel[ch_no].UF << ", Overflow: " << module.Channel[ch_no].OF  << ", Valid: " <<  module.Channel[ch_no].Valid << endl;
  }
  data = *m_iterator++;
  module.qdc_trailer = data;
  //debug->out(5) << "RCDRawEvent::Read792: QDC Trailer: " << std::hex << data << std::dec << endl;

  module.moduleRead = 1;
//  cout << ">>  Data Found !!!" << endl;
  v792_data.push_back(module);

  //TODO check trailer

  /*data = v792->output_buffer;
  dok = ((data >> 24) & 0x7);
  printf("EOB bits 24-27: 0x%x\n",dok);
  evnum = data & 0xffffff;
  */
}

 
void 
RCDRawEvent::Readv1290()
{
  u_int word, channel, wtype;
  double measurement;
  V1290Data data;
  data.Channel.resize(16);

  for (int i = 0; i < 16; i++) {
    data.Channel[i].Ncounts = 0;
  }
  if (*m_iterator==0) {
    //debug->out(4) << "RCDRawEvent::Readv1290 " << k << " : Empty event! " <<  endl;
    return;
  }
 
  word = *m_iterator++;
  wtype = (word & 0xf8000000) >> 27;
  if (wtype == 8) {                    // Global Header
    data.global_header = word;
    //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Global Header found! " << std::hex << word << std::dec <<  endl;
  } else {
    //debug->out(2) << "RCDRawEvent::Readv1290 " << k << " : Global Header not the first word! "  << std::hex << word << std::dec <<  endl;
  }
 
  // Loop over 2 HPTDC
  for (int i = 0; i < 2; i++) {
    word = *m_iterator++;
    wtype = (word & 0xf8000000) >> 27;
    if (wtype == 1) {                    // TDC Header
      //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : HPTDC Header found! "  << std::hex << word << std::dec <<  endl;
      data.hptdc_header[i] = word;
    } else {
      //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : HPTDC Header not the next word! " <<  std::hex << word << std::dec <<  endl;
    }
 
    while(1) {
      if (m_iterator == vec_raw.end()) {
        //debug->out(1) << "RCDRawEvent::Readv1290 " << k << " : "<< " End of event reached." << endl;
        return;
      }

      if (IsModuleHeader(*m_iterator)) {
        //debug->out(1) << "RCDRawEvent::Readv1290 " << k << " : "<< "Module header found instead of TDC Data! " << endl;
        return;
      }
 
      word = *m_iterator++;
      wtype = (word & 0xf8000000) >> 27;
      if (wtype == 0) {                    // TDC Header
        channel = (word & 0x3e00000) >> 21;
        measurement = (word & 0x1fffff);// in units of 25ps //*0.025;//ns

        // Writing data only if it is the first hit
        if(data.Channel[channel].Ncounts == 0 ) {
          data.Channel[channel].Data = measurement;
        }
        
        data.Channel[channel].MultiHit.push_back(measurement);
        data.Channel[channel].Ncounts += 1;
        //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : TDC measurement, channel: " <<  channel << ", data: " << measurement << ", counts: " <<  data.Channel[channel].Ncounts << endl;
      } else if (wtype == 3) {        // TDC trailer
        //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : HPTDC Trailer found! "  << std::hex << word << std::dec <<  endl;
        data.hptdc_trailer[i] = word;
        break;
      } else {
        //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Not a trailer or data word! "  << std::hex << word << std::dec <<  endl;
        //TODO exception
      }
    }
  }
 
  word = *m_iterator++;
  wtype = (word & 0xf8000000) >> 27;
  if (wtype == 16) {                    // Global Trailer
    data.global_trailer = word;
    //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Global Trailer found! " << std::hex << word << std::dec <<  endl;
  } else {
    //debug->out(5) << "RCDRawEvent::Readv1290 " << k << " : Global Trailer not the next word! "  << std::hex << word << std::dec <<  endl;
  }
  data.moduleRead = 1;
  v1290_data.push_back(data);
}
 

void 
RCDRawEvent::Readv560()
{
  u_int num, loop, ch_no, data;
  
  num = *m_iterator++;
  if (num > V560Data::num_channels ) {
    cerr << "RCDRawEvent::Readv560: event " << m_event_num << " invalid (too many channels)" << endl;
    return;
  }
  //debug->out(5) << "RCDRawEvent::Readv560: Number of Words in 560 data: " << num <<  endl;  
  V560Data module;
 
  for (loop = 0; loop < num; loop++) {
    data = *m_iterator++;
 
    //ch_no = (data >> 16) & 0x3f;
    ch_no = loop;
    //v560_data.Channel[ch_no].UF = (bool)(data >> 13) & 0x1;
    //v560_data.Channel[ch_no].OF = (bool)(data >> 12) & 0x1;
    module.Channel[ch_no].Data = data;
 
    //debug->out(5) << "RCDRawEvent::Read560: Scaler Word:" <<  std::hex << data << std::dec  <<  endl;
  }
  module.moduleRead = 1;
  v560_data.push_back(module);
}

//#define DEBUG 1
void 
RCDRawEvent::ReadMimosa()
{
  #ifdef DEBUG
  std::cout << "Inside RCDRawEvent::ReadMimosa()" << std::endl;
  std::cout << "Printing the fragment:" << std::endl;
  auto tmpIt = m_iterator;
  int counter = 0;
  do {
    std::cout << std::setfill('0') << std::setw(8) << std::hex << *tmpIt++ << " ";
    if(counter % 4 == 3) std::cout << std::endl;

    ++counter;
  } while((*(tmpIt - 1)) != 0xc0badebb); // As long as the last word processed was not C0BADEBB, continue printing
  std::cout << std::endl;
  #endif

  u_int size = (*m_iterator++)/4 - 3;
  auto nextModuleIterator = m_iterator + (size);

  u_int trailer = *(nextModuleIterator - 1);
  #ifdef DEBUG
  std::cout << "  Size: 0x" << size << " (in words), trailer: 0x" << trailer << std::endl;
  #endif
  if(trailer != EUDAQ_MIMOSA_TRAILER)
  {
    cerr << "RCDRawEvent::ReadMimosa: event " << m_event_num << " invalid (wrong trailer 0x" << std::hex << trailer << std::dec << ")" << endl;
    m_iterator = nextModuleIterator;
    #ifdef DEBUG
    std::cout << "Failure in RCDRawEvent::ReadMimosa()" << std::endl;
    #endif
    return;
  }

  if(size <= 2)
  {
    cerr << "RCDRawEvent::ReadMimosa: event " << m_event_num << " invalid (no frames have been read out)" << endl;
    m_iterator = nextModuleIterator;
    #ifdef DEBUG
    std::cout << "Failure in RCDRawEvent::ReadMimosa()" << std::endl;
    #endif
    return;
  }

  mimosa_data.push_back(readMimosaFrames());

  #ifdef DEBUG
  int count = 0;
  for(auto &tmp : mimosa_data)
    if(tmp.moduleRead) ++count;
  std::cout << "So far, " << count << " mimosa modules have been processed." << std::endl;
  #endif

  m_iterator = nextModuleIterator;

  #ifdef DEBUG
  std::cout << "The header for the next module is: 0x" << std::setfill('0') << std::setw(8) << std::hex << *m_iterator << std::endl;
  std::cout << "Exiting RCDRawEvent::ReadMimosa()" << std::endl;
  #endif
}

MimosaData
RCDRawEvent::readMimosaFrames()
{
  #ifdef DEBUG
  std::cout << "Inside RCDRawEvent::readMimosaFrames()" << std::endl;
  #endif

  MimosaData eudaqProducer;
  eudaqProducer.moduleRead = 0;

  if(!m_eudaqFlagsSet)
  {
    m_eudaqFlags.word = *(m_iterator + 2);
    m_eudaqFlagsSet = true;

    if(m_eudaqFlags.field.version != 0x1)
    {
      std::stringstream tmp;
      tmp << "RCDRawEvent::readMimosaFrames: wrong raw version, can understand version 0b0001; received 0b" << std::bitset<4>(m_eudaqFlags.field.version);
      throw std::runtime_error(tmp.str());
    }
  }

  #ifdef DEBUG
  auto tmpIt = m_iterator;
  uint32_t size = *(tmpIt+1);

  std::cout << "Frame 0 (size " << std::dec << size << "):" << std::endl;
  for(int i = 0; i < size; ++i)
  {
    std::cout << std::setfill('0') << std::setw(8) << std::hex << *tmpIt++ << " ";
    if(i % 4 == 3)
      std::cout << std::endl;
  }
  std::cout << std::endl;

  if(*tmpIt != 0xc0badebb)
  {
    size = *(tmpIt+1);

    std::cout << "Frame 1 (size " << std::dec << size << "):" << std::endl;
    for(int i = 0; i < size; ++i)
    {
      std::cout << std::setfill('0') << std::setw(8) << std::hex << *tmpIt++ << " ";
      if(i % 4 == 3)
        std::cout << std::endl;
    }
    std::cout << std::endl;
  }

  uint32_t word = *m_iterator;
  std::cout << "The IP is: " << word << " (" << std::setw(1) << std::dec << (word >> 24) << "." << ((word >> 16) & 0xff) << "." << ((word >> 8) & 0xff) << "." << (word & 0xff) << ")" << std::endl;
  #endif

  auto frame0Iterator = m_iterator;
  auto frame1Iterator = m_iterator;

  uint32_t senderIPAddress = *frame0Iterator++;
  uint32_t sizeWords0      = *frame0Iterator++;
  eudaqProducer.flags.word = *frame0Iterator++;
  eudaqProducer.evtNumber  = *frame0Iterator++;

  uint32_t readWords0 = 4;
  frame1Iterator += sizeWords0;
  int numFrames;
  if(*frame1Iterator == EUDAQ_MIMOSA_TRAILER)
    numFrames = 1;
  else
  {
    numFrames = 2;
    frame1Iterator += 4;
  }
  eudaqProducer.numFrames = numFrames;

  #ifdef DEBUG
  std::cout << "If the two frames exist, the iterators should point to the chip headers. If only one, then the second iterator should point to the IP word from the first frame:" << std::endl;
  std::cout << "Frame 0 Iterator: 0x" << std::setfill('0') << std::setw(8) << std::hex << *frame0Iterator << std::endl;
  std::cout << "Frame 1 Iterator: 0x" << std::setfill('0') << std::setw(8) << std::hex << *frame1Iterator << std::endl;
  #endif

  if(m_eudaqFlags.field.enabledPlanes != eudaqProducer.flags.field.enabledPlanes)
  {
    cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " invalid, the number of enabled mimosa planes is not consistent." << endl;
  }

  int sensor = 0;
  uint32_t pivot;
  do {
    #ifdef DEBUG
    std::cout << "Reading the sensor " << sensor << std::endl;
    #endif

    uint32_t header0 = *frame0Iterator++;
    uint32_t header1;
    if(numFrames == 2)
      header1 = *frame1Iterator++;
    ++readWords0;

    #ifdef DEBUG
    std::cout << "The header is: 0x" << std::setfill('0') << std::setw(8) << std::hex << header0 << std::endl;
    #endif

    if((header0 & 0xFFF0FFFF) != 0x55505555)
    {
      cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " invalid (one of the chips has a wrong header)" << endl;
      return std::move(eudaqProducer);
    }

    if(header0 != header1 && numFrames == 2)
    {
      cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " invalid (chip headers do not match in both frames)" << endl;
      return std::move(eudaqProducer);
    }

    MimosaPlane currentPlane;
    currentPlane.plane_id = header0;
    currentPlane.numFrames = numFrames;
    if(sensor == 0)
    {
      pivot = *frame0Iterator++;
      if(numFrames == 2)
        ++frame1Iterator;
      ++readWords0;
    }

    #ifdef DEBUG
    std::cout << "The unknown word (probably a frame counter) is: 0x" << std::setfill('0') << std::setw(8) << std::hex << *frame0Iterator << std::endl;
    #endif

    // This is the so-called WTF word (some sort of frame counter maybe)
    ++frame0Iterator;
    if(numFrames == 2)
      ++frame1Iterator;
    ++readWords0;

    #ifdef DEBUG
    std::cout << "The length of frame 0 is: 0x" << std::setfill('0') << std::setw(8) << std::hex << *frame0Iterator << std::endl;
    std::cout << "The length of frame 1 is: 0x" << std::setfill('0') << std::setw(8) << std::hex << *frame1Iterator << std::endl;
    #endif

    uint32_t len0 = *frame0Iterator++;
    uint32_t len1 = 0;
    if(numFrames == 2)
      len1 = *frame1Iterator++;
    ++readWords0;

    // Calculate real lengths
    if((len0 & 0xFFFF) != (len0 >> 16))
    {
      cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " warning, the two length indicators do not match for frame 0" << endl;
      len0 = std::max(len0 >> 16, len0 & 0xFFFF);
    }
    else
      len0 = len0 & 0xFFFF;
    if((len1 & 0xFFFF) != (len1 >> 16))
    {
      cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " warning, the two length indicators do not match for frame 1" << endl;
      len1 = std::max(len1 >> 16, len1 & 0xFFFF);
    }
    else
      len1 = len1 & 0xFFFF;

    // Read data
    readWords0 += DecodeMimosaData(currentPlane, frame0Iterator, len0);
    if(numFrames == 2)
      DecodeMimosaData(currentPlane, frame1Iterator, len1);

    uint32_t trailer0 = *frame0Iterator++;
    uint32_t trailer1 = 0;
    if(numFrames == 2)
      trailer1 = *frame1Iterator++;
    ++readWords0;

    if(trailer0 != trailer1 && numFrames == 2)
    {
      cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " invalid (chip trailers do not match in both frames)" << endl;
      return std::move(eudaqProducer);      
    }

    if(header0 & 0x000F0000 != trailer0 & 0x000F0000)
    {
      cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " invalid (the headers and trailers do not match)" << endl;
      return std::move(eudaqProducer);
    }

    eudaqProducer.planes.push_back(std::move(currentPlane));
    ++sensor;
  } while(readWords0 < sizeWords0);

  if(readWords0 != sizeWords0)
  {
    cerr << "RCDRawEvent::ReadMimosaFrames: event " << m_event_num << " invalid (the read size of the mimosa frame, " << readWords0 << ", does not match the reported size, " << sizeWords0 << ")" << endl;
    return std::move(eudaqProducer);
  }
  // Maybe add a check for sizeWords1

  eudaqProducer.moduleRead = 1;
  return std::move(eudaqProducer);
}

uint
RCDRawEvent::DecodeMimosaData(MimosaPlane &plane, vector <u_int>::iterator& iterator, uint32_t len)
{
  std::vector<uint16_t> stream;

  for(uint32_t i = 0; i < len; ++i)
  {
    uint32_t data = *iterator++;
    stream.push_back(data & 0xFFFF);
    stream.push_back(data >> 16);
  }

  auto streamIterator = stream.begin();

  while(streamIterator != stream.end())
  {
    uint16_t word = *streamIterator++;
    
    int numstates = word & 0xF;
    int row = (word >> 4) & 0x7FF;

    for(int i = 0; i < numstates; ++i)
    {
      if(streamIterator == stream.end())
      {
        cerr << "RCDRawEvent::DecodeMimosaData: event " << m_event_num << " invalid (there is a mismatch in size between the data stream and the number of states)" << endl;
        return len;
      }
      word = *streamIterator++;

      int num = word & 0x0003;
      int column = (word >> 2) & 0x07FF;

      // Create the pixels
      for(int j = 0; j < num + 1; ++j)
      {
        MimosaPixel pixel;
        pixel.x = column + j;
        pixel.y = row;
        // TODO: Add here pivot boolean if needed
        plane.pixels.push_back(std::move(pixel));
      }
    }
  }

  return len;
}

bool 
RCDRawEvent::IsModuleHeader(u_int word)
{
  switch(word) {
    case CAEN_TDC_v1x90x:
    case CAEN_QDC_v792x:
    case CAEN_SCALER_v560x:
    case EUDAQ_MIMOSA:
      return true;
    default:
      return false;
  }
  //if (word == V785_ModuleID || word == V792_ModuleID0 || word == V792_ModuleID1 || word == V1290_ModuleID0 || word == V1290_ModuleID1 || word == V560_ModuleID || word == Goniometer_ModuleID)
  //return true;
  //else
  //return false;
}

uint
RCDRawEvent::GetEventNumber(vector<u_int>  & vec_rawev)
{
  if (vec_rawev.size() < 9 || vec_rawev[0] != 0xee1234ee){
    throw std::runtime_error("RCDRawEvent: Malformed event");
  }
  return vec_rawev[5];
}
