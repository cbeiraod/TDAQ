#ifndef __EVENT_READER_BASE_H__
#define __EVENT_READER_BASE_H__

#include <vector>

class EventReaderBase {
  public:
    EventReaderBase() = delete;
    EventReaderBase(unsigned int verbosity):
        fVerbosity(verbosity), fRunNumber(-1), fNumEvents(0){};

    virtual std::vector<unsigned int> GetNextRawEvent() = 0;

    int GetRunNumber(){ return fRunNumber; }
    int GetNumberOfEvents(){ return fNumEvents; }
    void SetVerbosityLevel(unsigned int level){ fVerbosity = level; }
    
  protected:
    unsigned int fVerbosity;
    unsigned int fRunNumber;
    unsigned int fNumEvents;
};
#endif
