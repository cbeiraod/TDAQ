#include <stdexcept>

// File list generation
#include <regex>
#include <dirent.h>

#include "FileEventReader.h"
//#include "BL4SUtilities.h"

using namespace std;

FileEventReader::FileEventReader(string fname, bool multiFileReader,
                     int readNumEvents, int skipEvents, int firstFile): // BL4SConfig *cfg
  fMultiFile(multiFileReader),
  fIsFileOpen(false),
  fProcessNevents(readNumEvents),
  fSkipEvents(skipEvents),
  fFirstFile(firstFile),
  fNwords(0), fMemblock(nullptr), fMemblockSize(0), fIndex(0), fReadEvents(0),
  fNumberInSequence(-1), fIsFirstFile(true),
  EventReaderBase(0)
{
  ExtractRunNumber(fname);

if (multiFileReader){
    string dirname, initFilename; 
    
    // Split directory from file name
    size_t dirIndex = fname.find_last_of("/\\");
    if ( dirIndex == string::npos){
      dirname = ".";
      initFilename = fname;
    }else{
      dirname = fname.substr(0,dirIndex);
      initFilename = fname.substr(dirIndex+1);
    }
    
    // Verify file name format
    string fileSuffix("_[0-9]+\\.data$");
    regex reSuffix(".*"+fileSuffix);
    
    if (! regex_match(initFilename, reSuffix)){
      throw std::invalid_argument(initFilename + ": expected .*_NNNN.data");
    }
    
    initFilename=regex_replace(initFilename, regex(fileSuffix), "");
    regex reFilename(initFilename + fileSuffix);
    
    // Generate list of files for current run
    DIR *dir = opendir(dirname.c_str());
    if ( dir == nullptr ){
      throw std::runtime_error( dirname + ": access denied");
    }
    struct dirent *entry = readdir(dir);

    auto extractFileNum = [](string fname) -> int
    {
      int pos = fname.length() - 9;
      return stoi(fname.substr(pos, 4));
    };
    
    while (entry != nullptr){
      if (entry->d_type != DT_DIR && regex_match(entry->d_name, reFilename)){
          string fn = dirname + "/" + entry->d_name;
          if ( extractFileNum(fn) >= fFirstFile ){
            fFiles.push_back(make_pair(fn, 0)); 
          }
      }
      entry = readdir(dir);
    }
    closedir(dir);

    // Check if files in dataset are missing and extract available
    // and saved number of events
    auto cmpFileEnt = [](const pair<string, int> &p1,
                         const pair<string, int> &p2) -> bool
    {
      return (p1.first < p2.first);
    };
    
    fFiles.sort(cmpFileEnt);
    
    int nextFileNum = fFirstFile;
    bool firstMissingFile = true;
    for (auto &fn: fFiles){
      
      // Calculate number of events and update file list
      fNumEvents += GetNumEventsFromFile(fn.first);
      fn.second = fNumEvents - 1;
      cout << "FileEventReader| Add " << fn.first << " ("
           << fn.second << ")" << endl;
      int fileNumber = extractFileNum(fn.first);
      if (fileNumber != nextFileNum && fFiles.size() != 1 ){
        if (firstMissingFile == true){
          cerr << "WARN FileEventReader| Dataset incomplete\n";
        }
        cerr << "WARN FileEventReader| Missing file number(s) ";
        for (int i=nextFileNum; i<fileNumber; i++){
          cerr << i << ", ";
        }
        cerr << endl;
        firstMissingFile = false;
      }
      nextFileNum = fileNumber + 1;
    }
  }else{
    fNumEvents = GetNumEventsFromFile(fname);
    fFiles.push_back(make_pair(fname, fNumEvents));
  }
   
  if (fFiles.empty()){
    throw std::runtime_error("internal error: no files added to queue");
  }

  cout << "FileEventReader| Files added to queue: "
       << fFiles.size() << endl;
  cout << "FileEventReader| Events advertised: "
       << GetNumEventsFromFile(fFiles.back().first, true) << endl;
  cout << "FileEventReader| Events available: "
       << fNumEvents << endl;
}
    
void
FileEventReader::OpenFile(string fname)
{

  if (fIsFileOpen){
    fRawFile.close();
    fIsFileOpen = false;
  }
  
  cout << "FileEventReader| Open " << fname << endl;
  fRawFile.open(fname.c_str()); // Opening the file
  if (!fRawFile.is_open()) {
    throw std::runtime_error(fname + ": access denied");
  }
  fIsFileOpen = true;
  fIndex = 0;

  fRawFile.seekg(0,ios::end);
  size_t file_size = fRawFile.tellg();
  fNwords = file_size/sizeof(u_int);
  if ( file_size > fMemblockSize){
    if (fMemblock != nullptr){
      delete fMemblock;
      fMemblock = nullptr;
    }
    fMemblock = new u_int[fNwords];
    fMemblockSize = fNwords;
  }
  fRawFile.seekg (0, ios::beg);
  fRawFile.read ((char*)&fMemblock[0], file_size);//fNwords);
  fRawFile.close();

  ReadHeaderFooter();
  //debug->out(2) << "File Size = " << file_size << endl;
  //debug->out(2) << "fNwords = " << fNwords << endl;
  //for (int i = 0 ; i < fNwords-1 ; i++)
  //{ cout << std::hex << fMemblock[i] << endl;}
}


void 
FileEventReader::ReadHeaderFooter()
{
  //debug->out(2) << "FileEventReader::ReadHeaderFooter(): Reading File Header and Footer" << endl;
  u_int date,time;
  char datetimestring[20];

  // Checking if first word is start Marker, exiting otherwise
  //BL4SException ex;
  if (fMemblock[0]!=0x1234aaaa) {
    cout << "First word is not 0x1234aaaa as expected! Not a valid file!!!  " << endl;
    exit(EXIT_FAILURE);
  }

  fNumberInSequence = fMemblock[3];
  date = fMemblock[4];
  time = fMemblock[5];

  sprintf(datetimestring, "%02d.%02d.%04d, %02d:%02d:%02d",
      date/1000000,(date%1000000)/10000,date%10000,time/10000,(time%10000)/100,time%100);

  string open_datetime = string(datetimestring);
  if (fIsFirstFile){
    fOpenTimestamp = GetTimestampFromString(open_datetime);
  //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File opening date-time : " << open_datetime << endl;
  //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File opening timestamp : " << fOpenTimestamp<< endl;
  }

  // Reading the footer
  string close_datetime;
  if (fMemblock[fNwords-10] != 0x1234dddd) {
    //cout << "WARNING!!! -6th word is not 0x1234eeee as expected! Footer not written properly?" << endl;
    close_datetime = "UNKNOWN";
    fCloseTimestamp = -1;
  } else {
    date = fMemblock[fNwords-8];
    time =  fMemblock[fNwords-7];
    sprintf(datetimestring,"%02d.%02d.%04d, %02d:%02d:%02d",date/1000000,(date%1000000)/10000,date%10000,time/10000,(time%10000)/100,time%100);

    close_datetime = string(datetimestring);
    fCloseTimestamp = GetTimestampFromString(close_datetime);
    //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File closing date-time : " << close_datetime << endl;
    //debug->out(2) <<"FileEventReader::ReadHeaderFooter(): File closing timestamp: " << fCloseTimestamp << endl;

    u_int filesize_in_MB =  fMemblock[fNwords-5];
    u_int nevents_run =  fMemblock[fNwords-4];
    u_int runsize_in_MB =  fMemblock[fNwords-3];
    u_int status =   fMemblock[fNwords-2];
    u_int endmarker =   fMemblock[fNwords-1]; //Should be 0x1234eeee
  }
}


vector<u_int>
FileEventReader::GetNextRawEvent()
{
  //debug->out(4) << "FileEventReader::GetNextRawEvent: " <<  " Entered GetNextRawEvent" << endl;
  //BL4SException ex;
  if (! fIsFileOpen){
    if (! fFiles.empty()){
      // Remove files from list which will be skipped (skipEvents)
      for (auto it= fFiles.begin(); it!=fFiles.end(); ++it){
        if (it->second > fSkipEvents){
          fFiles.erase(fFiles.begin(), it);
          break;
        }
        fReadEvents = it->second;
      }

      OpenFile(fFiles.front().first);
      fFiles.pop_front();
      fFirstFile = false;
    }else{
      throw std::out_of_range("access beyond end of last file");
      }
    }
  
  if (fIndex >= fNwords) {
      throw std::out_of_range("access beyond memory block boundaries");
    }

  u_int event_size = 0;
  do {
    if ( fProcessNevents != -1 && fReadEvents >= fProcessNevents + fSkipEvents ){
      cerr << "FileEventReader::GetNextRawEvent(): "
           << "Read all requested " << fProcessNevents << " events! "
           << "Returning an empty event." << endl;
      return vector<u_int>();
    }

    fIndex += event_size;
    while (fMemblock[fIndex] != 0x1234cccc) {
      fIndex++;
      //debug->out(5) << "FileEventReader::GetNextRawEvent: " << fIndex << " " << std::hex << fMemblock[fIndex] << std::dec << endl;

      if (fIndex >= fNwords) {
        if (! fFiles.empty()){
          OpenFile(fFiles.front().first);
          fFiles.pop_front();
          fFirstFile = false;
      }else{
        cerr << "FileEventReader::GetNextRawEvent(): Reached the end of memory block! Returning an empty event." << endl;
        vector<u_int> empty;
        return empty;
        }
      }
    }
    // Going to the word with size in bytes
    fIndex += 3;
    event_size = fMemblock[fIndex]/sizeof(u_int);
    fReadEvents++;
  } while ( fReadEvents <= fSkipEvents );

  vector <u_int> vec_rawev(&fMemblock[fIndex+1], &fMemblock[fIndex+1] + event_size);
  fIndex += event_size;
  //debug->out(4) << "FileEventReader::GetNextRawEvent: " <<  " Event found, returning" << endl;
  return vec_rawev;
}

/*
void 
FileEventReader::SkipEvent(bool nodebug)
{
  //debug->out(4) << "FileEventReader::SkipEvent: " << "Entered Skip Event" << endl;
  //BL4SException ex;
  
  if (fIndex >= fNwords) {
     throw std::out_of_range("access beyond memory block boundaries");
  }

  while (fMemblock[fIndex] != 0x1234cccc) {
    fIndex++;
    //debug->out(5) << "FileEventReader::SkipEvent: " << fIndex << " "  << std::hex << fMemblock[fIndex] << std::dec << endl;
    if (fIndex > fNwords) {
      //debug->out(1) << "FileEventReader::SkipEvent: fIndex exceeded event file size! "  << endl;
      throw;
    }
  }

  fIndex++;
  while(fMemblock[fIndex] != 0x1234cccc) {
    fIndex++;
    //debug->out(5) << "FileEventReader::SkipEvent: " << fIndex << " " << std::hex << fMemblock[fIndex] << std::dec << endl;
    if (fIndex > fNwords) {
      throw;
    }
  }
  fIndex--;
  //debug->out(4) << "FileEventReader::SkipEvent: " << "Skipped 1 Event" << endl;
  //if (nodebug) {
    //debug->SetVerbosity(verbosedummy);
    //}
}
*/

FileEventReader::~FileEventReader()
{
  //debug->out(2) << "FileEventReader::~FileEventReader: Reading file is finished!!!" << endl;
  //delete debug;
    //delete[] fMemblock;
}


time_t 
FileEventReader::GetTimestampFromString(string datetimestring)
{
    struct tm tmm;

    strptime(datetimestring.c_str(), "%d.%m.%Y, %H:%M:%S", &tmm);
    time_t tt = mktime(&tmm);
    return tt; // TODO There is a problem because of daylight saving time, sometimes it seems like I should substract one hour, sometime it doesn't. I'll check it again

}


void
FileEventReader::ShowFileInfo()
{
}

int
FileEventReader::ExtractRunNumber(string fname)
{
  //data_test.1407245718.calibration_T9test.daq.RAW._lb0000._BL4SApp._0002.data
  //Finding the first . in the file name. Normally filenames are as data_test.1241231251.....data
  size_t found = fname.find("data_test.");

  if (found == std::string::npos) { // If space is not found ignoring
    //debug->out(1) << "WARNING!!! FileEventReader::GetfRunNumber: Can not get run number, returning 0" << endl;
    return -1;
  }
  try {
    fRunNumber = std::stoi(fname.substr(found+10,10));
  }
  catch(...) {
    fRunNumber = -1;
  }
  //debug->out(2) << "FileEventReader::GetfRunNumber: Run Number:" << fRunNumber << endl;

  return fRunNumber;
}

u_int
FileEventReader::GetNumEventsFromFile(string fname, bool total)
{
  std::ifstream inFile;
  inFile.open(fname.c_str());
  if (!inFile.is_open()) {
    throw std::runtime_error(fname + ": access denied");
  }

  int offset = (total) ? 4 : 6;
  // Prepare for catastrophe
  string errMsg("FileEventReader::GetNumEventsFromFile: Invalid data in ");
  errMsg += fname;

  // Get number of words
  inFile.seekg(0,ios::end);
  int endPos = inFile.tellg();
  int nWords = endPos/sizeof(u_int);

  // Check if file is complete and extract number of events from footer
  u_int currentWord;
  if ( nWords > 19 ){
     inFile.seekg(-4, ios_base::end);
     inFile.read ((char*)&currentWord, 4);
     if ( currentWord != 0x1234eeee ){ 
       throw std::runtime_error(errMsg.c_str());
     }
     inFile.seekg(offset * -4, ios_base::end);
     inFile.read((char*)&currentWord, 4);
     return currentWord;
    inFile.close();
  }else{
       inFile.close();
       throw std::runtime_error(errMsg.c_str());
  }
}
