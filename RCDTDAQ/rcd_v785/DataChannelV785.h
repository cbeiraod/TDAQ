/********************************************************/
/*							*/
/* Date: 31 October 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2015 - The software with that certain something **/

#ifndef DATACHANNELV785_H
#define DATACHANNELV785_H

#include "ROSInfo/DataChannelV785Info.h"
#include "ROSCore/SingleFragmentDataChannel.h"

namespace ROS 
{
  class DataChannelV785 : public SingleFragmentDataChannel 
  {
  public:    
    DataChannelV785(u_int id,
		    u_int configId,
		    u_int rolPhysicalAddress,
		    u_long vmeBaseVA,
		    DFCountedPointer<Config> configuration,
		    DataChannelV785Info* = new DataChannelV785Info());
    virtual ~DataChannelV785() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, u_long pciAddress = 0);
    void clearInfo(void);
    virtual ISInfo *getISInfo(void);

  private:
    v785_regs_t *m_v785;
    DataChannelV785Info *m_statistics;  //for IS
    u_int m_channel_number;    
    u_int m_channelId;
    
    enum Statuswords 
    {
      S_OK = 0,
      S_TIMEOUT,
      S_OVERFLOW,
      S_NODATA
    };
  };
}
#endif //DATACHANNELV785_H
