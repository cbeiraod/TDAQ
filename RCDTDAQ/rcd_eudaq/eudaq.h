/********************************************************/
/* Header file for the Eudaq implementation		*/ 
/*							*/
/* Date: 05.06.2019					*/ 
/* Author: C. Beirao / P. Schuetze			*/
/*							*/
/*** C 2015 - The software with that certain something **/

#ifndef _EUDAQ_H
#define _EUDAQ_H

/*************/
/* Constants */
/*************/
#define EUDAQ_CHANNELS 1     //This represents a UDP link to the Eudaq ?
const u_int EUDAQ_MIMOSA = 0x800;
const u_int EUDAQ_MIMOSA_TRAILER = 0xC0BADEBB;


/*************************/
/* Parameter definitions */
/*************************/

/* No longer used, but kept for reference
struct mimosaBitmaskBits {
  unsigned plane0 : 1;
  unsigned plane1 : 1;
  unsigned plane2 : 1;
  unsigned plane3 : 1;
  unsigned plane4 : 1;
  unsigned plane5 : 1;
};

union mimosaBitmask {
  unsigned all : 6;
  mimosaBitmaskBits bits : 6;
};*/

struct eudaqFlagFields {
  unsigned enabledPlanes : 6;
  unsigned  : 2;
  unsigned  : 8;
  unsigned  : 8;
  unsigned frame : 2;
  unsigned  : 2;
  unsigned version : 4;
};

union eudaqFlags {
  uint32_t word;
  eudaqFlagFields field;
};

#endif
