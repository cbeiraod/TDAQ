/********************************************************/
/*							*/
/* Date: 05.06.2019					*/ 
/* Author: C. Beirao / P. Schuetze			*/
/*							*/
/*** C 2015 - The software with that certain something **/

#ifndef DATACHANNELEUDAQ_H
#define DATACHANNELEUDAQ_H

#include "ROSInfo/DataChannelEUDAQInfo.h"
#include "ROSCore/SingleFragmentDataChannel.h"

namespace ROS 
{
  class DataChannelEUDAQ : public SingleFragmentDataChannel 
  {
  public:    
    DataChannelEUDAQ(u_int id,
		    u_int configId,
		    int eudaqport,
		    DFCountedPointer<Config> configuration,
		    DataChannelEUDAQInfo* = new DataChannelEUDAQInfo());
    virtual ~DataChannelEUDAQ() ;
    virtual int getNextFragment(u_int* buffer, int max_size, u_int* status, u_long pciAddress = 0);
    void clearInfo(void);
    virtual ISInfo *getISInfo(void);
    void resetEventCounter(){m_nEvents = 0;}

  private:
    DataChannelEUDAQInfo *m_statistics;  //for IS
    u_int m_channel_number;    
    u_int m_channelId;
    int m_socket;
    u_int m_nFEs;
    u_int m_nEvents;
    u_int m_busyDelay;
    
    enum Statuswords 
    {
      S_OK = 0,
      S_TIMEOUT,
      S_OVERFLOW,
      S_NODATA,
      S_BADFES
    };
  };
}
#endif //DATACHANNELEUDAQ_H
