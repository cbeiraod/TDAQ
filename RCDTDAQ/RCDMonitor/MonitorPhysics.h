#ifndef __MONITOR_PHYSICS__ 
#define __MONITOR_PHYSICS__ 

#include "MonitorBase.h"
#include "HistogramStore.h"

#include <vector>


class MonitorPhysics: public MonitorBase {
  public:
    MonitorPhysics();
    MonitorPhysics(std::string name);
    ~MonitorPhysics();

   	virtual bool Process(RCDRawEvent& ev);
   	virtual void Print(RCDRawEvent& ev);

  private:
    const int fMaxTDC;
    const int fMaxQDC;

};

  
#endif
