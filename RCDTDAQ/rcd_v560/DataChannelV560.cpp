//$Id: DataChannelV560.cpp 38498 2007-07-13 15:27:39Z joos $
/********************************************************/
/*							*/
/* Date: 17 November 2006				*/ 
/* Author: Markus Joos, J.O.Petersen			*/
/*							*/
/*** C 2007 - The software with that certain something **/


#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/Config.h"
#include "rcd_v560/ExceptionV560.h"
#include "rcd_v560/v560.h"
#include "rcd_v560/DataChannelV560.h"


using namespace ROS;
using namespace RCD;


/**********************************************************************************************/
DataChannelV560::DataChannelV560(u_int channelId, 
                                 u_int channelIndex, 
                                 u_int rolPhysicalAddress,
				 u_long vmeBaseVA, 
                                 u_int numberOfScalerChannels,
				 DFCountedPointer<Config> configuration) :
  SingleFragmentDataChannel(channelId, channelIndex, rolPhysicalAddress, configuration)
/**********************************************************************************************/
{ 
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::constructor: Entered");
  m_v560 = reinterpret_cast<v560_regs_t*>(vmeBaseVA);  // (virtual) base address of the module
  m_channelId = channelId;
  m_rolPhysicalAddress = rolPhysicalAddress;
    
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelV560::constructor: channel ID = " << m_channelId << " rolPhysicalAddress = " << m_rolPhysicalAddress );
  m_numberOfScalerChannels = numberOfScalerChannels;
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 20, "DataChannelV560::constructor: # scaler channels = " << m_numberOfScalerChannels);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::constructor: Done");
}


/*********************************/
DataChannelV560::~DataChannelV560() 
/*********************************/
{
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::destructor: Entered");
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::destructor: Done");
}


/************************************************************************************************/
int DataChannelV560::getNextFragment(u_int* buffer, int max_size, u_int* status,unsigned long pciAddress)
/************************************************************************************************/
{  
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::getNextFragment: Entered for channel = " << m_rolPhysicalAddress);
  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::getNextFragment: max_size = " << max_size);

  u_int *cptr, loop, fsize = 0;
  u_int *bufPtr = buffer;        // the buffer address

  // channel ID as block identifier
  *bufPtr++ = m_channelId;	         
  fsize += 4;

  *bufPtr++ = CAEN_SCALER_v560x; // Write the module type
  fsize += 4;

  //Write the number of data words;
  *bufPtr++ = m_numberOfScalerChannels;
  fsize += 4;

  cptr = (u_int *)&m_v560->counter0; 
  for (loop = 0; loop < m_numberOfScalerChannels; loop++)
  {
    if (fsize == (u_int)max_size)
    {  
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 5, "DataChannelV560::getNextFragment: max_size limit reached on channel " << m_rolPhysicalAddress << ". Data will be truncated now.");
      DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::leaving getNextFragment with fsize = " << fsize);
      *status = S_OVERFLOW;
      return fsize;	// bytes ..
    }
    *bufPtr++ = cptr[loop];
    fsize += 4;
  }

  DEBUG_TEXT(DFDB_RCDEXAMPLE, 15, "DataChannelV560::leaving getNextFragment with fsize = " << fsize);
  *status = S_OK;
  return fsize;	// bytes ..
}


/*************************************************/
DFCountedPointer<Config> DataChannelV560::getInfo() 
/*************************************************/
{
   DFCountedPointer<Config> info = Config::New();
   
   return(info);
}
