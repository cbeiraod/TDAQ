/********************************************************/
/* Header file for the CAEN V560			*/ 
/*							*/
/* Date: 17 November 2006				*/ 
/* Author: Markus Joos					*/
/*							*/
/*** C 2006 - The software with that certain something **/

#ifndef _V560_H
#define _V560_H

/*************/
/* Constants */
/*************/
const u_int CAEN_SCALER_v560x = 0x400;

/********************/
/* Type definitions */
/********************/
struct v560_regs_struct_t
{
  u_int dummy1;                            //0x00
  u_short interrupt_vector;                //0x04
  u_short interrupt_level_and_veto;        //0x06
  u_short enble_vme_interrupt;             //0x08
  u_short disble_vme_interrupt;            //0x0A
  u_short clear_vme_interrupt;             //0x0C
  u_short request;                         //0x0E
  u_int counter0;                          //0x10
  u_int counter1;                          //0x14
  u_int counter2;                          //0x18
  u_int counter3;                          //0x1C
  u_int counter4;                          //0x20
  u_int counter5;                          //0x24
  u_int counter6;                          //0x28
  u_int counter7;                          //0x2C
  u_int counter8;                          //0x30
  u_int counter9;                          //0x34
  u_int counter10;                         //0x38
  u_int counter11;                         //0x3C
  u_int counter12;                         //0x40
  u_int counter13;                         //0x44
  u_int counter14;                         //0x48
  u_int counter15;                         //0x4C
  u_short scale_clear;                     //0x50
  u_short vme_veto_set;                    //0x52
  u_short vme_veto_reset;                  //0x54
  u_short scale_increase;                  //0x56
  u_short scale_status;                    //0x58
  u_short dummy2[80];                      //0x5A - 0xF8
  u_short fixed_code;                      //0xFA
  u_short manufacturer_module_type;        //0xFC
  u_short version_series;                  //0xFE
};

using v560_regs_t = volatile v560_regs_struct_t;

#endif
