#ifndef __MONITOR_SCALER__ 
#define __MONITOR_SCALER__ 

#include <vector>

#include "CommonLibrary/MonitorBase.h"

class BL4SConfig;
class TH1I;
class TFile;
class TGraph;

class MonitorScaler: public MonitorBase {
  public:
    MonitorScaler();
    MonitorScaler(std::string name, BL4SConfig* cfg, std::string runNumber, bool publish);
    ~MonitorScaler();

    virtual bool Process(RCDRawEvent& ev);
    virtual void Print(RCDRawEvent& ev);
    virtual void WriteToFile();
    virtual void PublishHists();
    virtual void PrepareRootFile(std::string runNumberStr);


  private:
    std::map<std::string, int> m_scaler_channel_map;
    std::vector<TGraph*> m_scVsTime;
    TH1I * m_scaler_counts;
    int m_channelNoTimer;
    int m_graphCount;
    TFile * m_outputFile;
    std::string m_MonitorName;


};

  
#endif
