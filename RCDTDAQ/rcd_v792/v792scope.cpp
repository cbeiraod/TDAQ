/************************************************************************/
/*									*/
/* This is a simple prototyping program for the CAEN V792		*/
/*									*/
/* Date:   31 October 2006						*/
/* Author: Markus Joos							*/
/*									*/
/**************** C 2006 - A nickel program worth a dime ****************/

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"
#include "rcd_v792/v792.h"


/**************/
/* Prototypes */
/**************/
void mainhelp(void);
void regdecode(void);
void ronedata(void);
void revent(void);
void configure(void);
void dumpth(void);

/***********/
/* Globals */
/***********/
v792_regs_t *v792;

/*****************************/
int main(int argc, char **argv)
/*****************************/
{
  int handle;
  u_int havebase = 0, mode = 0, ret, v792_base = 0x00100000, fun = 1;
  unsigned long value;
  char c;
  static VME_MasterMap_t master_map = {0x00100000, 0x10000, VME_A32, 0};

  while ((c = getopt(argc, argv, "db:")) != -1)
  {
    switch (c) 
    {
      case 'b':	
      {
        v792_base  = strtol(optarg, 0, 16); 
	havebase = 1;
      }
      break;
      case 'd': mode = 1;                           break;
      default:
	printf("Invalid option %c\n", c);
	printf("Usage: %s  [options]: \n", argv[0]);
	printf("Valid options are ...\n");
	printf("-b <VME A32 base address>: The hexadecimal A32 base address of the V792\n");
	printf("-d:                        Dump registers and exit\n");
	printf("\n");
	exit(-1);
    }
  }
  
  if (!havebase)
  {
    printf("Enter the VMEbus A32 base address of the V792\n");
    v792_base = gethexd(v792_base);
  }
  master_map.vmebus_address = v792_base;

  ret = VME_Open();
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMap(&master_map, &handle);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }

  ret = VME_MasterMapVirtualLongAddress(handle, &value);
  if (ret != VME_SUCCESS)
  {
    VME_ErrorPrint(ret);
    exit(-1);
  }
  v792 = (v792_regs_t *) value;
  
  if (mode ==1)
  {
    regdecode();
  }
  else
  {
    while (fun != 0)  
    {
      printf("\n");
      printf("Select an option:\n");
      printf("   1 Help\n");  
      printf("   2 Decode registers\n");  
      printf("   3 Read one data word\n");  
      printf("   4 Configure the card for data taking\n");  
      printf("   5 Read one event\n");  
      printf("   6 Dump the threshold memory\n");  
      printf("   0 Exit\n");
      printf("Your choice ");
      fun = getdecd(fun);
      if (fun == 1) mainhelp();
      if (fun == 2) regdecode();
      if (fun == 3) ronedata();
      if (fun == 4) configure();
      if (fun == 5) revent();
      if (fun == 6) dumpth();
   }  
  } 
   
  ret = VME_MasterUnmap(handle);
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  
  ret = VME_Close();
  if (ret != VME_SUCCESS)
    VME_ErrorPrint(ret);
  exit(0);
}


/*****************/
void mainhelp(void)
/*****************/
{
  printf("\n=========================================================================\n");
  printf("Contact markus.joos@cern.ch if you need help\n");
  printf("=========================================================================\n\n");
}


/******************/
void regdecode(void)
/******************/
{
  u_short value, value2, value3;
  u_int geo_slot, data;

  printf("\n=========================================================================\n");
  //printf("output_buffer     is at 0x%08x\n", (u_int)&v792->output_buffer     - (u_int)&v792->output_buffer);
  //printf("status_register_2 is at 0x%08x\n", (u_int)&v792->status_register_2 - (u_int)&v792->output_buffer);
  //printf("thresholds        is at 0x%08x\n", (u_int)&v792->thresholds[0]     - (u_int)&v792->output_buffer);
  //printf("oui_msb           is at 0x%08x\n", (u_int)&v792->oui_msb           - (u_int)&v792->output_buffer);
  //printf("revision          is at 0x%08x\n", (u_int)&v792->revision          - (u_int)&v792->output_buffer);
  //printf("serial_lsb        is at 0x%08x\n", (u_int)&v792->serial_lsb        - (u_int)&v792->output_buffer);

  printf("ROM data:\n");
  value = v792->oui_msb;  value &= 0xff;
  value2 = v792->oui;     value2 &= 0xff;
  value3 = v792->oui_lsb; value3 &= 0xff;
  data = (value << 16) | (value2 << 8) | value3;
  printf("Manufacturer identifier:     %d (0x%08x)\n", data, data);
  
  value = v792->version;  value &= 0xff;
  printf("Board version:               %d (0x%02x)\n", value, value);

  value = v792->board_id_msb;   value &= 0xff;
  value2 = v792->board_id;      value2 &= 0xff;
  value3 = v792->board_id_lsb;  value3 &= 0xff;
  data = (value << 16) | (value2 << 8) | value3;
  printf("Board ID:                    %d (0x%08x)\n", data, data); 
  
  value = v792->revision;  value &= 0xff;
  printf("Board revision:              %d (0x%02x)\n", value, value);
  
  value = v792->serial_msb;     value &= 0xff;
  value2 = v792->serial_lsb;    value2 &= 0xff;
  data = (value << 8) | value2; 
  printf("Board Serial number:         %d (0x%08x)\n", data, data); 

  printf("\nR/W register data:\n");
  geo_slot = v792->geo_address & 0x1f;
  if (geo_slot == 0x1f)
    printf("The card has no PAUX connector. Therefore the slot number defaults to 31\n");
  else
    printf("The card is installed in slot %d\n", geo_slot);

  printf("\nFirmware revision:           0x%04x\n", v792->firmware_revision);
  
  value = v792->status_register_1;
  printf("\nDecoding the Status register 1 (0x%04x)\n", value);
  printf("Data ready:                  %s\n", (value & 0x001)?"Yes":"No");
  printf("Board is busy:               %s\n", (value & 0x004)?"Yes":"No");
  printf("Event trigger register flag: %s\n", (value & 0x100)?"Set":"Not set");

  printf("\nEvent trigger register:      0x%04x\n", v792->event_trigger_register);

  value = v792->status_register_2;
  printf("\nDecoding the Status register 2 (0x%04x)\n", value);
  printf("Buffer empty:                %s\n", (value & 0x002)?"Yes":"No");
  printf("Buffer full:                 %s\n", (value & 0x004)?"Yes":"No");

  value = v792->event_counter_l;
  value2 = v792->event_counter_h;
  data = ((value2 &0xff) << 16) | value;
  printf("\nEvent counter:               %d\n", data);

  printf("\nBit set 1 register:          0x%04x\n", v792->bit_set_1);
  printf("\nBit set 2 register:          0x%04x\n", v792->bit_set_2);
  printf("\nCrate number:                0x%04x\n", v792->crate_select);
  
  printf("\nCommon pedestal current register: 0x%02x\n", v792->iped);
  
  printf("=========================================================================\n\n");
}


/*****************/
void ronedata(void)
/*****************/
{
  u_int wtype, data;

  printf("\n=========================================================================\n");
  data = v792->output_buffer;
  printf("Raw data = 0x%08x\n", data);
  wtype = (data >> 24) & 0x7;

  printf("wtype = %d\n", wtype);

  if (wtype == 0x0) 
  {
    printf("This is a valid data word\n");
    printf("GEO             = %d\n", data >> 27);
    printf("Channel         = %d\n", (data >> 16) & 0x3f);
    printf("Under threshold = %s\n", (data & 0x2000)?"Yes":"No");
    printf("Overflow        = %s\n", (data & 0x1000)?"Yes":"No");
    printf("ADC data        = 0x%03x\n", data &0xfff);
  }

  else if (wtype == 0x2) 
  {
    printf("This is a header word\n");
    printf("GEO                         = %d\n", data >> 27);
    printf("crate                       = %d\n", (data >> 16) &0xff);
    printf("numer of channels with data = %d\n", (data >> 8) & 0x3f);
  }

  else if (wtype == 0x4) 
  {
    printf("This is a end of block\n");
    printf("GEO             = %d\n", data >> 27);
    printf("Event counter   = %d\n", data &0xffffff);
  }

  else if (wtype == 0x6) 
    printf("This is an invalid data word\n");

  else 
    printf("Error: Invalid word type (0x%01x)\n", wtype);
  printf("=========================================================================\n\n");
}


/***************/
void revent(void)
/***************/
{
  u_int dok, slot, crate, evnum, loop, num, valid[32], un[32], ov[32], evdata[32], data;
  u_short value;
  
  printf("\n=========================================================================\n");
  while (1)
  {
    value = v792->status_register_2;
    if (value & 0x2)
    {
      printf("ERROR: Data buffer empty. No complete event found\n");
      return;
    }
    data = v792->output_buffer;
    if (((data >> 24) & 0x7) == 2) //header found
      break;
  }
  
  crate = (data >> 16) & 0xff;
  slot = data >> 27;
  
  num = (data >> 8) & 0x3f;
  for (loop = 0; loop < num; loop++)
  {
    data = v792->output_buffer;
    evdata[(data >> 16) & 0x3f] = data & 0xfff;
    un[(data >> 16) & 0x3f]     = (data >> 13) & 0x1;
    ov[(data >> 16) & 0x3f]     = (data >> 12) & 0x1;
    dok = (data >> 24) & 0x7;
    if (dok)
      valid[(data >> 16) & 0x3f] = 0;
    else  
      valid[(data >> 16) & 0x3f] = 1;
  }
  
  evnum = v792->output_buffer & 0xffffff;

  printf("Event received from crate    = %d, slot = %d\n", crate, slot);
  printf("Event number                 = %d\n", evnum);
  printf("Number of channels with data = %d\n", num);
  printf("Channel | valid | UN | OV |     Data\n");
  printf("========|=======|====|====|=========\n");
  for (loop = 0; loop < num; loop++)
  {
    printf("     %2d |", loop);
    printf("   %s |", valid[loop]?"Yes":" No");
    printf("  %d |", un[loop]);
    printf("  %d |", ov[loop]);
    printf(" 0x%06x\n", evdata[loop]);
  }
  printf("=========================================================================\n\n");
}


/***************/
void dumpth(void)
/***************/
{
  u_int loop;
  u_short data;
  
  printf("Channel | Masked | Threshold\n");
  for (loop = 0; loop < V792_CHANNELS; loop++)
  {
    data = v792->thresholds[loop];
    printf("     %2d |", loop);
    printf("    %s |", (data & 0x100)?"Yes":" No");
    printf("      0x%02x\n", data &0xff);
  }
}


/******************/
void configure(void)
/******************/
{  
  u_int m_iped = 0;

  printf("Enter the common pedestal current: ");
  m_iped = getdecd(m_iped);

  //Initialize the V792 card
  v792->bit_set_1    = 0x80;         //start soft reset
  v792->bit_clear_1  = 0x90;         //end soft reset & disable ADER
  v792->crate_select = 1;            //Set (dummy) crate number
  v792->bit_set_2    = 0x18;         //disable over range and under threshold filters
  v792->bit_set_2    = 0x4;          //initiate clear data
  v792->bit_clear_2  = 0x4;          //complete clear data
  v792->iped         = m_iped;       //common pedestal current
  
  //What comes now may not be necessary as I am just programming the module with its default
  //values. However, it clarifies how he module will be used
  v792->interrupt_level        = 0;  //Disable the interrupt 
  v792->interrupt_vector       = 0;  //Disable the interrupt
  v792->event_trigger_register = 0;  //Disable the interrupt
  v792->event_counter_reset    = 0;  //Reset event counter
  
  for (u_int loop = 0; loop < V792_CHANNELS; loop++)
    v792->thresholds[loop] = 0;      //Initialize the threshold memory
}





