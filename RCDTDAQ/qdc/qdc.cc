/**************************************************/
/*  QDC V792  Monitor                             */
/*                                                */
/*  monitor for ONE QDC V792                      */
/*                                                */
/*  2017/09/1 modernise a la mmChamber            */
/*  author:  J.Petersen                           */
/**************************************************/

#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <iostream>
#include <string>

#include <cmdl/cmdargs.h>
#include <ipc/core.h>
#include <oh/OHRootProvider.h>

#include "CommonLibrary/DAQEventReader.h"
#include "CommonLibrary/FileEventReader.h"
#include "EventLooper.h"
#include "BL4SConfig.h"

template<typename EventReader, typename Source>
void ProcessEvents(Source, int nEvents);

void 
term_handler(int param) {
  assert(param == 15);
  std::cout << "Handling SIGTERM... exiting" << std::endl;
  exit(0);
}

int 
main(int argc, char** argv)
{
  CmdArgStr             partition_name                  ('p', "partition", "partition-name", "partition to work in." );
  CmdArgInt             events                          ('e', "events", "events-number", "number of events to retrieve (default 10000)\n"
                                                         "-1 means work forever" );
  CmdArgInt             publish_update_cycle_seconds    ('u', "update", "publish_update_cycle_seconds", "number of events to be processed before publish (default 10000)\n"
                                                         "-1 means work forever" );
  CmdArgInt             verbosity                       ('v', "verbosity", "verbosity-level",   " 0 - print nothing (default)\n"
                                                         " 1 - print event number and event size\n"
                                                         " 2 - print event number, event size and event data.\n"
                                                         "-1 - print value of the first word of the event.");
  CmdArgStr             cfg_file_name                   ('c', "cfg_file_name", "cfg_file_name", "Config file name");
                    
  CmdArgStr             datafilename                    ('f', "datafilename", "asynchronous", "Data file name, if entered it reads events from the file instead of online stream");
  CmdArgSet              multiFileRun                   ('m', "multirun", "Run over all files of a run (offline) or continuously over run boundaries (online)");  
  
  events = -1;
  verbosity = 0;
  publish_update_cycle_seconds = 5;
  partition_name = getenv("TDAQ_PARTITION");
  cfg_file_name = "current_config.cfg";
  multiFileRun = true;    // !!!
    
  CmdArgvIter arg_iter( --argc, ++argv );
  CmdLine cmd(*argv, &partition_name, &events, &verbosity,
              &publish_update_cycle_seconds, &cfg_file_name, &datafilename, &multiFileRun,
              NULL);
  cmd.description( "QDC Monitoring" );
  cmd.parse( arg_iter );
    
  //if(cfg_file_name.isNULL())
    //cfg_file_name = config_file_ss.str().c_str();

  try {
    IPCCore::init( argc, argv );
  } catch( daq::ipc::Exception & ex ) {
    ers::fatal( ex );
  }

  signal(SIGTERM, term_handler);
  IPCPartition partition(partition_name);

  //Initializing BL4S classes
  //Opening config file 
  std::string config(cfg_file_name);
  BL4SConfig * cfg = new BL4SConfig(config);
  cfg->ShowConfig();

  EventLooper evLoop;
  evLoop.SetConfigFile(cfg);
 
  if(datafilename.isNULL()) {
    cout << "Running online" << endl;
    OHRootProvider provider = OHRootProvider(partition, "Histogramming", "QDCMonitor");
    evLoop.SetHistProvider(provider);
    evLoop.SetPublishCycleTime(publish_update_cycle_seconds);
    evLoop.ProcessEvents<DAQEventReader, IPCPartition>(partition, events, multiFileRun);
  } else { 
    evLoop.ProcessEvents<FileEventReader, string>(string(datafilename), events, multiFileRun);
  }

  return 0;
}
