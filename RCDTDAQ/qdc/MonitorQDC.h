#ifndef __MONITOR_QDC__ 
#define __MONITOR_QDC__ 

#include <vector>

#include "CommonLibrary/MonitorBase.h"

class BL4SConfig;
class TH1I;
class TFile;

class MonitorQdc: public MonitorBase {
  public:
    MonitorQdc();
    MonitorQdc(std::string name, BL4SConfig* cfg, std::string runNumber, bool publish);
    ~MonitorQdc();

    virtual bool Process(RCDRawEvent& ev);
    virtual void Print(RCDRawEvent& ev);
    virtual void WriteToFile();
    virtual void PublishHists();
    virtual void PrepareRootFile(std::string runNumberStr);

  private:
    TFile * m_outputFile;
    std::string m_MonitorName;
    std::vector<TH1I*> m_QDC;

};

  
#endif
