#ifndef __MONITOR_MIMOSA__ 
#define __MONITOR_MIMOSA__ 

#include <vector>
#include <map>

#include "CommonLibrary/MonitorBase.h"

class BL4SConfig;
class TH2D;
class TFile;

class MonitorMimosa: public MonitorBase {
  public:
    MonitorMimosa();
    MonitorMimosa(std::string name, BL4SConfig* cfg, std::string runNumber, bool publish);
    ~MonitorMimosa();

    virtual bool Process(RCDRawEvent& ev);
    virtual void Print(RCDRawEvent& ev);
    virtual void WriteToFile();
    virtual void PublishHists();
    virtual void PrepareRootFile(std::string runNumberStr);

  private:
    TFile * m_outputFile;
    std::string m_MonitorName;
    std::vector<TH2D*> m_MIMOSA;
    std::map<u_int,size_t> idMapping;

};

  
#endif
