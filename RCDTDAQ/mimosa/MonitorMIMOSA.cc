#include <TH2D.h>
#include <TFile.h>

#include <oh/OHRootProvider.h>

#include "MonitorMIMOSA.h"
#include "BL4SConfig.h"
#include "CommonLibrary/RCDRawEvent.h"

using namespace std;

MonitorMimosa::MonitorMimosa(std::string name, BL4SConfig * cfg, 
                             std::string runNumberStr, bool publish )
{
  m_MonitorName = name;

  // BEFORE booking histograms
  if (!publish) {
    PrepareRootFile(runNumberStr);
  }

/********booking histograms**************/
/* copied from last year ...  */

// MIMOSAs
  for (int plane=0; plane<6; plane++) {
    char name[8], title[200];
    sprintf(name, "mimosa0-%02d", plane);
    sprintf(title, "plane %d;x pixel;ypixel", plane);
    m_MIMOSA.push_back(new TH2D(name, title, 1152, -0.5, 1151.5, 576, -0.5, 575.5));
  }

}

MonitorMimosa::~MonitorMimosa()
{
}

//#define DEBUG
bool
MonitorMimosa::Process(RCDRawEvent& ev)
{
  #ifdef DEBUG
  std::cout << "Processing an event" << std::endl;
  std::cout << "  The event has " << ev.mimosa_data.size() << " mimosa modules" << std::endl;
  #endif

  #ifdef DEBUG
  int moduleCounter = 0;
  #endif
  for(auto &mimosaData : ev.mimosa_data) {   // one module
    #ifdef DEBUG
    std::cout << "    The module " << moduleCounter << " has the following information:" << std::endl;
    #endif
    if(!mimosaData.moduleRead)
    {
      #ifdef DEBUG
      std::cout << "      The module was not successfully read" << std::endl;
      #endif
      continue;
    }
    #ifdef DEBUG
    std::cout << "      The module was successfully read" << std::endl;
    std::cout << "      The module has read " << mimosaData.numFrames << " frames" << std::endl;
    std::cout << "      The module has read " << mimosaData.planes.size() << " planes" << std::endl;
    #endif

    if(mimosaData.numFrames != 2)
      continue;

    for(auto &mimosaPlane : mimosaData.planes)
    {
      #ifdef DEBUG
      std::cout << "        This plane has ID " << std::hex << mimosaPlane.plane_id << std::dec << std::endl;
      #endif

      // If a certain plane id has not yet been mapped to a given index
      if(idMapping.find(mimosaPlane.plane_id) == idMapping.end())
      {
        /*int newIndex = 0;
        for(auto &map : idMapping)
        {
          if(map.second >= newIndex)
            newIndex = map.second + 1;
        }*/

        // Please note that it is important that the following command be separated into two steps
        // If condensed into one, the new element of the map would be created first so the index would be wrong by one
        int newIndex = idMapping.size();
        idMapping[mimosaPlane.plane_id] = newIndex;

        // Then update the name of the histogram when drawn
        std::string oldName = m_MIMOSA[idMapping[mimosaPlane.plane_id]]->GetTitle();
        std::string newName = oldName.substr(0, 7);
        newName += " - Plane ID: 0x";
        std::stringstream idStr;
        idStr << std::hex << mimosaPlane.plane_id;
        newName += idStr.str();
        newName += oldName.substr(7);
        m_MIMOSA[idMapping[mimosaPlane.plane_id]]->SetTitle(newName.c_str());
      }

      for(auto &pixel : mimosaPlane.pixels)
      {
        m_MIMOSA[idMapping[mimosaPlane.plane_id]]->Fill(pixel.x, pixel.y);
      }
    }
  }

 return true;
}

void
MonitorMimosa::Print(RCDRawEvent& ev)
{
  /*cout << std::dec << "MIMOSA Modules: " << ev.v792_data.size() << "\n";
  for(auto mimosaData : ev.v792_data) {
    int channel = 0;
    for(auto mimosaChannel : mimosaData.Channel) {
        cout << "Ch: " << channel
             << ", Value: " << mimosaChannel.Data
             << endl;
      ++channel;
    }
  }*/

}

void
MonitorMimosa::WriteToFile() {

  m_outputFile->Write();
  m_outputFile->Close();
}

void
MonitorMimosa::PublishHists() {

  int plane = 0;
  for(auto mimosaHists : m_MIMOSA) {
    m_histProvider->publish(*m_MIMOSA[plane],m_MIMOSA[plane]->GetName());
    ++plane;
  }
  
}

void MonitorMimosa::PrepareRootFile(std::string runNumberStr) {

// Store histograms in root file mirroring the OH hierarchy
// construct the ROOT file name here in 'user' space
// BEFORE booking histograms

  TDirectory * histogramming_dir = 0;
  TDirectory * monitor_dir = 0;

  const char * histogramming = "Histogramming";
  const char * monitor = "Mimosa_monitor";
  string output_name;

  //output_name.append("/afs/cern.ch/work/d/daquser/public/bl4smonitor/");
  output_name.append("/home/bl4sdaq/TDAQ/Data/");
  output_name.append(runNumberStr);
  output_name.append(monitor);
  output_name.append(".root");
  printf(" MonitorMimosa::prepareRootFile, Output file name: %s\n", output_name.c_str());
  m_outputFile = new TFile(output_name.c_str(), "RECREATE");
// give Tfile a structure
  histogramming_dir = m_outputFile->mkdir(histogramming);
  histogramming_dir->cd();
  monitor_dir = histogramming_dir->mkdir(monitor);
  monitor_dir->cd();    // current directory

}

