# Beamline for Schools - RCDTDAQ

This repository contains the BL4S flavour of the RCDTDAQ readout software: 
* TDAQ plugins for the CAEN VME modules and MicroMegas readout
* Monitoring programmes
* Configuration files


### IMPORTANT: Please read the [README](https://gitlab.cern.ch/BL4S/Documents) file from the BL4S/Documents repository first </h1>

## Structure of the repository

* **Configs:** Non-TDAQ configuration files
* **OKS:** Stored TDAQ OKS databases
  * **current:** Currently active configuration
* **RCDTDAQ:** Code base of the BL4S specific readout code
  * **CommonLibrary:** Event handling classes
  * **EventDumper:** Low level event dumper (Good starting point to learn how to 
                     access/process raw data)
  * **RCDMonitor:** Monitoring plugin for TDAQ
  * **external:** Additional libraries from other projects (e.g. MicroMegas DAQ)
  * **installed:** Binaries, header and active configuration files
     * **x86_64-slc6-gcc49-opt:** Binaries and libraries
     * **share:** Configuration files (OKS schemas and link to the current config in /OKS) 
  * **microMegas/rcd_\***: Detector specific readout code
  * **x86_64-slc6-gcc49-opt:** CMake build folder

## Configuration
### TDAQ build and runtime environment
The BL4S code is build upon the ATLAS TDAQ infrastructure, which has to be 
located either locally on the workstation or in AFS.
Information about TDAQ installation paths and software versions have to be 
provided in `/RCDTDAQ/RCDTDAQ_settings.sh`.
An example file with the current defaults can be found in the same folder.  

The startup script will use the the example file, if the settings file is not 
found.

### OKS Databases
These databases hold the configuration of the DAQ system and are located in
`/OKS` with the currently active one in `/OKS/current`.

In file RCDSegment.data.xml please check correctness of the following: 
- **DirectoryToWrite@RCD\_Storage:** Path for raw data.
- **RunsOn@RCDApp:** The computer accessing to VME e.g. SBC
- **Contains@RCDApp:** Readout modules

Furthermore in the file partRCDTDAQ.data.xml check:
- **RepositoryRoot@Partition:** Installation path of the RCD modules
- **DefaultHost@Partition:**
- **DefaultTags@Partition:** especially if the compiler or the system has been 
                             changed
 
## Building and running the DAQ
* After cloning the repository, first pull the external libraries
   ```
   git submodule init
   git submodule update
   ```
   
* Create `RCDTDAQ_settings.sh` by copying the example file and adapt it to your 
  setup

* Check the OKS databases in `/OKS/current`  
   If the folder is empty, copy one of the configurations from the surrounding
   folders to `current` and adapt it.

* Prepare your environment and build the code
   ```
   source setup_RCDTDAQ.sh
   ./build.sh
   ```

* Start the TDAQ infrastructure (This step may fail sometimes. Usually a second 
  try works.)
  ```
  ./setup_RCDTDAQ.sh start
  ```

* Run the graphical interface
  ```
  Igui_start
  ```

## Development
#### Code contribution
Contributing code shall be done via feature branches in order to keep the 
master branch compiling and running at any time. 
When you start a new development, create a new branch called 
`yourNick-descriptiveName`. 
Once you want to open the code for discussion, review  or merging into the 
master branch create a `merge request` and ask somebody to approve it.  

Create atomic commits with a descriptive message. `git add -p` is a helpful 
tool to review your code and split it up. Individual commits in a series do not 
need to be functional if they clarify your intentions.

#### Configuration files
Configuration files and developments for a specific project of a year should be
located in branches. The branch name should obey the following name convention: 
  `BL4S_YEAR` or `BL4S_YEAR_projectName`.  

In order to add a branch use:

```
git branch BRANCHNAME
```
To switch between branches use:
```
git checkout BRANCHNAME
```
