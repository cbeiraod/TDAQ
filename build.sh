#!/bin/bash

cat << "EOF"
                                                  
__/\\\\\\\\\\\\\____/\\\________________________/\\\________/\\\\\\\\\\\___        
 _\/\\\/////////\\\_\/\\\______________________/\\\\\______/\\\/////////\\\_       
  _\/\\\_______\/\\\_\/\\\____________________/\\\/\\\_____\//\\\______\///__      
   _\/\\\\\\\\\\\\\\__\/\\\__________________/\\\/\/\\\______\////\\\_________     
    _\/\\\/////////\\\_\/\\\________________/\\\/__\/\\\_________\////\\\______    
     _\/\\\_______\/\\\_\/\\\______________/\\\\\\\\\\\\\\\\_________\////\\\___   
      _\/\\\_______\/\\\_\/\\\_____________\///////////\\\//___/\\\______\//\\\__  
       _\/\\\\\\\\\\\\\/__\/\\\\\\\\\\\\\\\___________\/\\\____\///\\\\\\\\\\\/___ 
        _\/////////////____\///////////////____________\///_______\///////////_____
                                                  
EOF

ROOT=$(cd `dirname $0`; pwd -P)
cd $ROOT

function run()
{
  msg "info" "Running '$1'...\n"
  eval "$1"
  ERROR_CODE=$?
  if [ "$ERROR_CODE" != '0' ]; then
    msg "error" "Command '$1' has failed\n"
    exit $ERROR_CODE
  fi
  msg "info" "Command '$1' run successfully\n"
}

function usage() {
  echo "USAGE: $0 [-d LEVEL] [-j JOBS] [-r] [-- make arguments]" >&2
  echo "       -d LEVEL : Debug build" >&2
  echo "       -j JOBS  : Number of parallel build processes (make -j parameter)" >&2
  echo "       -r       : Rebuild" >&2
}

while getopts ":d:j:r" opt; do
  case $opt in
    r)
      rebuild=true
      ;;
    d)
      if ! [[ "$OPTARG" =~ ^[0-9]$ ]]; then
        usage
        exit 2
      fi
      if [ -n "${TDAQ_PARTITION}" ]; then
        echo "ERROR: Debug level cannot be changed after RCDTDAQ has been set up" >&2
        echo "       Please reset the shell or start with a new one" >&2
        exit 2
      fi
      setupArgs="debug $OPTARG"
      ;;
    j)
      if ! [[ "$OPTARG" =~ ^[0-9]+$ ]]; then
        usage
        exit 2
      fi
      makeJobs=$OPTARG
      ;;
    *)
      usage
      exit 2
      ;;
  esac
done
shift $((OPTIND - 1))
makeArgs="$*"
[ -n "$makeJobs" ] && makeArgs="${makeArgs} -j $makeJobs"

source setup_RCDTDAQ.sh $setupArgs
source RCDTDAQ_settings.sh

buildPath=${ROOT}/RCDTDAQ/${CMTCONFIG}

mkdir -p ${buildPath}
cd ${buildPath}

if [ $rebuild ]; then
  echo "Cleaning ${buildPath}"

  tdaqInstPath=${ROOT}/RCDTDAQ/installed
  rm -rf ${buildPath}/* \
         ${tdaqInstPath}/${CMTCONFIG} \
         ${tdaqInstPath}/include
  unset tdaqInstPath
fi

if [ "$BUILD_TYPE" == "DBG" ]; then
  msg "info" "debug build\n"
  run "cmake .. \
   -DCMAKE_CXX_FLAGS=\"-DDEBUG_LEVEL=${TDAQ_ERS_DEBUG_LEVEL}\""
else
  msg "info" "optimized build\n"
  run "cmake .. "
fi

#run "cmake .. \
#-DCMAKE_CXX_COMPILER=${LOCAL_ATLAS_TDAQ_INST_DIR}/LCG_81c/gcc/4.9.3/x86_64-slc6/bin/g++ \
#-DCMAKE_C_COMPILER=${LOCAL_ATLAS_TDAQ_INST_DIR}/LCG_81c/gcc/4.9.3/x86_64-slc6/bin/gcc \
#-DCMAKE_INSTALL_PREFIX=$ROOT/RCDTDAQ/installed/${CMTCONFIG}"
#run "make VERBOSE=1"
run "make install $makeArgs"
