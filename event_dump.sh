#!/bin/bash

ROOT=$(cd `dirname $0`; pwd -P)

if [ -z "$TDAQ_PARTITION" ]; then
  source setup_RCDTDAQ.sh 
fi
export TDAQ_ERS_LOG="null"

event_dump $@
